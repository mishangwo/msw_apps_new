#encoding=utf8
'''
生成订单报表
'''

from pymongo import MongoClient
import tablib, datetime

MONGO_DB_HOST = 'localhost'
MONGO_DB_PORT = 27017
MONGO_DB_NAME = 'msw'

def dump(group_no):
    dataset = []
    headers = ['Email', 'sku', 'type', 'order_no', 'price', 'status', 'created_at']
    mongo = MongoClient(host=MONGO_DB_HOST, port=MONGO_DB_PORT)[MONGO_DB_NAME]
    query = {
            'test_group': str(group_no),
            'created_at': {
                '$gte': datetime.datetime(2015, 5, 7, 21, 0, 0),
                '$lte': datetime.datetime(2015, 5, 10, 23, 59, 59)
                }
            }
    for order in mongo.order.find(query).sort([('_id', 1)]):
        row = [order['email'], order['sku'], order['_type'], order['out_trade_no'], order['total_fee']/100, order['status'], order['created_at'].strftime('%Y-%m-%d %H:%M:%S')]
        dataset.append(row)

    data = tablib.Dataset(*dataset, headers=headers)
    with open('/tmp/group_%s.xlsx' % group_no, 'wb') as f:
        f.write(data.xlsx)

def _get_all_buyer_email():
    buyer_set = set()
    mongo = MongoClient(host=MONGO_DB_HOST, port=MONGO_DB_PORT)[MONGO_DB_NAME]
    query = {
            'status': {
                    '$in': [3, 4, 5]
                },
            }
    for order in mongo.order.find(query).sort([('_id', 1)]):
        buyer_set.add(order['email'])

    return buyer_set

def get_all_buyer():
    dataset = []
    headers = ['Email', 'sku', 'type', 'order_no', 'price', 'created_at']
    mongo = MongoClient(host=MONGO_DB_HOST, port=MONGO_DB_PORT)[MONGO_DB_NAME]
    query = {
            'status': {
                    '$in': [3, 4, 5]
                },
            'created_at': {
                '$gte': datetime.datetime(2015, 5, 7, 21, 0, 0),
                '$lte': datetime.datetime(2015, 5, 10, 23, 59, 59)
                }
            }
    for order in mongo.order.find(query).sort([('_id', 1)]):
        row = [order['email'], order['sku'], order['_type'], order['out_trade_no'], order['total_fee']/100, order['created_at'].strftime('%Y-%m-%d %H:%M:%S')]
        dataset.append(row)

    data = tablib.Dataset(*dataset, headers=headers)
    with open('/tmp/buyers.xlsx', 'wb') as f:
        f.write(data.xlsx)

def get_potential_buyer():
    buyers = _get_all_buyer_email()
    dataset = []
    headers = ['Email', 'sku', 'type', 'order_no', 'price', 'status', 'created_at']
    mongo = MongoClient(host=MONGO_DB_HOST, port=MONGO_DB_PORT)[MONGO_DB_NAME]
    query = {
            'status': {
                    '$in': [1, 2]
                },
            'created_at': {
                '$gte': datetime.datetime(2015, 5, 7, 21, 0, 0),
                '$lte': datetime.datetime(2015, 5, 10, 23, 59, 59)
                }
            }
    for order in mongo.order.find(query).sort([('_id', 1)]):
        email = order['email']
        if email in buyers:
            continue
        row = [order['email'], order['sku'], order['_type'], order['out_trade_no'], order['total_fee']/100, order['status'], order['created_at'].strftime('%Y-%m-%d %H:%M:%S')]
        dataset.append(row)

    data = tablib.Dataset(*dataset, headers=headers)
    with open('/tmp/potential_buyers.xlsx', 'wb') as f:
        f.write(data.xlsx)

if __name__ == '__main__':
    dump(1)
    dump(2)
    dump(3)
    dump(4)

    get_all_buyer()

    get_potential_buyer()
