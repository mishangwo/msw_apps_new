#encoding=utf8

import sys
import os
reload(sys)
sys.setdefaultencoding('utf-8')
'''
生成活动订单报表
'''

from pymongo import MongoClient
import tablib, datetime

MONGO_DB_HOST = 'msw_mongo01'
MONGO_DB_PORT = 27017
MONGO_DB_NAME = 'msw'

def dump(sku):
    dataset = []
    headers = ['sku', '课程名', '用户名', '手机号码', '邮箱','订单号', '支付类型','支付金额']
    mongo = MongoClient(host=MONGO_DB_HOST, port=MONGO_DB_PORT)[MONGO_DB_NAME]
    query = {
            'status': {'$in': [3, 4, 5]},
            'total_fee': 50000,
            'sku': sku
            }
    for order in mongo.activities.find(query).sort([('_id', 1)]):
        row = [order['sku'], order['body'], order['username'], order['telephone'], order['email'], order['out_trade_no'], order['_type'], order['total_fee']/100]
        dataset.append(row)

    data = tablib.Dataset(*dataset, headers=headers)
    with open(os.getcwd() + '/script/activities_%s.xlsx' % sku, 'wb') as f:
        f.write(data.xlsx)


if __name__ == '__main__':
    dump('SDWH')
