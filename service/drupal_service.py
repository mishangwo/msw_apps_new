# coding: utf-8
from __future__ import division

from pymongo_conn import get_mongodb_conn
import datetime
import requests
import json
from config import DP_CREATE_ORDER_URL, DP_API_KEY, DP_GET_CSRF_TOKEN_URL


class Drupal_Service(object):
    def __init__(self):
        self.mongo = get_mongodb_conn()
        self.s = requests.Session()
        self.csrf_token = self._get_csrf_token()

        self._init_session()

    def _init_session(self):
        self.s.headers.update({'Api-Key': DP_API_KEY, 'X-CSRF-Token': self.csrf_token, 'Content-Type': 'application/json'})

    def _get_csrf_token(self):
        return requests.get(DP_GET_CSRF_TOKEN_URL, verify=True).content

    def post_to_drupal(self, order, transaction_id, gateway, channel):
        _now = datetime.datetime.now()
        _out_trade_no = order['out_trade_no']
        data = {
            'email': order['email'].lower(),
            'created': datetime.datetime.strftime(_now, '%Y-%m-%d %H:%M:%S'),
            'order_id': _out_trade_no,
            'order_status': 'completed',
            'products': [
                {
                    'sku': order['sku'],
                    'name': order['body'],
                    'price': int(order['total_fee']) / 100,
                    'qty': '1'
                },
            ],
            'payment': {
                'gateway': gateway,
                'channel': channel,
                'chanscation': transaction_id,
                'amount': int(order['total_fee']) / 100,
                'currency': 'CNY',
                'ip_address': order['spbill_create_ip'],
                'extra_data': ''
            }
        }

        if not 'source' in order:
            data['source'] = 'priv'
        else:
            data['source'] = order['source']

        status_code = -1
        resp_content = ''
        data = json.dumps(data)
        _exec = ''
        succeed = True

        MAX_TRY_COUNT = 3
        try_count = 0
        while try_count < MAX_TRY_COUNT:
            try_count += 1
            try:
                resp = self.s.post(DP_CREATE_ORDER_URL, data=data, verify=True)
                status_code = resp.status_code
                resp_content = resp.content
                _exec = ''
                succeed = True
                break
            except Exception, e:
                _exec = str(e)
                succeed = False
                print 'Error sending data to drupal [Try time: %d], [%s] %s' % (try_count, _out_trade_no, _exec)
        # 把结果保存到数据库
        drupal_data = {
            'out_trade_no': _out_trade_no,
            'post_data': data,
            'status_code': status_code,
            'resp_content': resp_content,
            'exec': _exec,
            'succeed': succeed,
            'try_count': try_count,
            'time': _now
        }
        self.mongo.drupal_process_record.insert(drupal_data)
        return resp_content
