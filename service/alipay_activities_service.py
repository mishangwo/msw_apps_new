# coding: utf8

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import time
import random
import string
import hashlib
import re
import urllib
import requests
from config import (
    ALIPAY_PARTNER,
    ALIPAY_KEY,
    ALIPAY_SERVICE_CREATE,
    ALIPAY_VERSION,
    ALIPAY_FORMAT,
    ALIPAY_SEC_ID,
    ALIPAY_SELLER_ACCOUNT_NAME,
    ALIPAY_ACTIVITIES_CALLBACK_URL,
    ALIPAY_ACTIVITIES_NOTIFY_URL,
    ALIPAY_URL,
    ALIPAY_SERVICE_PAY
)

from config import (
    ALIPAY_V2_PARTNER,
    ALIPAY_V2_SELLER_ID,
    ALIPAY_V2_KEY,
    ALIPAY_V2_INPUT_CHARSET,
    ALIPAY_V2_SERVICE,
    ALIPAY_V2_SIGN_TYPE,
    ALIPAY_V2_NOTIFY_URL,
    ALIPAY_V2_RETURN_URL,
    ALIPAY_V2_PAYMENT_TYPE,
    ALIPAY_V2_GATEWAY,
)
from pymongo_conn import get_mongodb_conn
from common_service import Common_Service


class Alipay_Activities_Service(object):
    def __init__(self):
        self.mongo = get_mongodb_conn()

    def _create_nonce_str(self):
        return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(15))

    def _create_timestamp(self):
        return int(time.time())

    def _create_alipay_signature(self, params):
        '''
        获取生成订单的签名
        '''
        string = '&'.join(['%s=%s' % (key, params[key]) for key in sorted(params)])
        string += ('%s' % ALIPAY_KEY)
        signature = hashlib.md5(string).hexdigest()
        return signature

    def _create_alipay_req_data_by_order_info(self, order_info):
        req_data_dict = {
            'subject': order_info['out_trade_no'],
            'out_trade_no': order_info['out_trade_no'],
            'total_fee': order_info['total_fee'] / 100.0,
            'seller_account_name': ALIPAY_SELLER_ACCOUNT_NAME,
            'call_back_url': ALIPAY_ACTIVITIES_CALLBACK_URL,
            'notify_url': ALIPAY_ACTIVITIES_NOTIFY_URL,
        }
        req_data_xml = Common_Service.dict_to_alipay_req_data(req_data_dict)
        return req_data_xml

    def get_request_token(self, order_info):
        '''
        根据订单信息，生成支付宝支付授权接口需要的数据，调用授权接口，返回request token
        这个步骤其实和微信的create pre pay差不多
        '''
        params = {
            'service': ALIPAY_SERVICE_CREATE,
            'format': ALIPAY_FORMAT,
            'v': ALIPAY_VERSION,
            'partner': ALIPAY_PARTNER,
            'req_id': str(order_info['_id']),
            'sec_id': ALIPAY_SEC_ID,
            'req_data': self._create_alipay_req_data_by_order_info(order_info),
        }
        sign = self._create_alipay_signature(params)
        params['sign'] = sign

        resp = requests.post(ALIPAY_URL, data=params)
        resp = urllib.unquote(resp.content)
        p = re.compile(r'<request_token>(\w+)</request_token>')
        token_list = p.findall(resp)
        if token_list and len(token_list) == 1:
            return True, token_list[0]
        else:
            return False, u'获取支付宝授权Token失败'

    def get_alipay_execute_url(self, token):
        params = {
            'service': ALIPAY_SERVICE_PAY,
            'format': ALIPAY_FORMAT,
            'v': ALIPAY_VERSION,
            'partner': ALIPAY_PARTNER,
            'sec_id': ALIPAY_SEC_ID,
            'req_data': '<auth_and_execute_req><request_token>%s</request_token></auth_and_execute_req>' % token,
        }
        sign = self._create_alipay_signature(params)
        params['sign'] = sign

        param_string = '&'.join(['%s=%s' % (key, params[key]) for key in sorted(params)])
        return ALIPAY_URL + '?' + param_string

