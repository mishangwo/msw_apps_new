# encoding=utf8
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from pymongo_conn import get_mongodb_conn
import datetime
import hashlib
import time
from config import (
    ACT_INFO,
    ORDER_STATUS
)


class Activities_Service(object):

    def __init__(self):
        self.mongo = get_mongodb_conn()


    def get_order_by_out_trade_no(self, out_trade_no):
        '''
        根据订单号返回订单详情
        '''
        return self.mongo.activities.find_one({'out_trade_no': out_trade_no})


    def update_order_status_by_otn(self, out_trade_no, status):
        '''
        根据订单号修改订单的状态
        '''
        search_dict = {
            'out_trade_no': out_trade_no
        }

        if status == ORDER_STATUS.WEIXIN_PAID_SUCCEED:
            update_dict = {
                '$set': {
                    'status': status,
                    'paid_at': datetime.datetime.now()
                }
            }
        else:
            update_dict = {
                '$set': {
                    'status': status,
                }
            }
        self.mongo.activities.update(search_dict, update_dict)

    def create_order(self, email, username, telephone, openid, sku, spbill_create_ip, _type):
        '''
        创建订单
        Params
            email: 用户提交的Email
            openid: 用户的openid，如果在微信内授权购买，可获得此参数，否则为空
            sku: sku信息
            spbill_create_ip: 用户IP
            _type: 订单支付类型，alipay，yeepay或weixin等等
            test_group: 标识该订单来自那个测试组
        '''

        sku_info = ACT_INFO[sku]

        _now = datetime.datetime.now()
        random_str = hashlib.md5(str(time.time())).hexdigest()[:4].upper()
        out_trade_no = sku + datetime.datetime.strftime(_now, '%Y%m%d%H%M%S') + random_str

        order_data = {
            'openid': openid,
            'body': sku_info['body'],
            'out_trade_no': out_trade_no,
            'total_fee': sku_info['price'],
            'spbill_create_ip': spbill_create_ip,
            '_type': _type,
            'status': ORDER_STATUS.NEW_CREATED,
            'created_at': datetime.datetime.now(),
            'email': email,
            'username': username,
            'telephone': telephone,
            'sku': sku
        }

        self.mongo.activities.insert(order_data)
        return True, order_data
