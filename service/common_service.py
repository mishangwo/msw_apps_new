# coding: utf-8
'''
通用方法类
'''
from bottle import request

import xml.etree.ElementTree as ET


class Common_Service(object):
    @staticmethod
    def dict_to_xml(dic):
        xml = ["<xml>"]
        for k, v in dic.iteritems():
            v = str(v)
            if v.isdigit():
                xml.append("<{0}>{1}</{0}>".format(k, v))
            else:
                xml.append("<{0}><![CDATA[{1}]]></{0}>".format(k, v))
        xml.append("</xml>")
        return "".join(xml)

    @staticmethod
    def dict_to_alipay_req_data(dic):
        xml = ["<direct_trade_create_req>"]
        for k, v in dic.iteritems():
            v = str(v)
            xml.append("<{0}>{1}</{0}>".format(k, v))
        xml.append("</direct_trade_create_req>")
        return "".join(xml)

    @staticmethod
    def xml_to_dict(xml):
        dic = {}
        root = ET.fromstring(xml)
        for child in root:
            value = child.text
            dic[child.tag] = value
        return dic

    @staticmethod
    def is_android():
        return request.headers.get('User-Agent', '').find('Android') >= 0

    @staticmethod
    def is_wechat():
        return request.headers.get('User-Agent', '').lower().find('micromessenger') >= 0

    @staticmethod
    def is_ios():
        return request.headers.get('User-Agent', '').find('iPhone') >= 0 or request.headers.get('User-Agent', '').find('iPad') >= 0

    @staticmethod
    def is_mobile():
        return Common_Service.is_android() or Common_Service.is_ios()
