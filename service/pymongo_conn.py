# coding: utf-8

'''
获取到MongoDB的连接
'''

from pymongo import MongoClient
from config import MONGO_DB_HOST, MONGO_DB_PORT, MONGO_DB_NAME

def get_mongodb_conn():
    return MongoClient(host=MONGO_DB_HOST, port=MONGO_DB_PORT)[MONGO_DB_NAME]
