# coding: utf-8

from config import SKU_INFO

class SkuService(object):
    @classmethod
    def get_sku_info_by_sku_code(cls, sku_code):
        return SKU_INFO.get(sku_code)
