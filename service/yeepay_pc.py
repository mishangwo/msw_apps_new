# coding: utf-8

import hmac

from config import (
    YEEPAY_PC_MERID,
    YEEPAY_PC_REQ_URL,
    YEEPAY_PC_MERKEY,
    YEEPAY_NETBANK_CALLBACK_URL
)


class YeepayPcService(object):
    @classmethod
    def get_request_url(cls, transid, total_fee, bank_id, title):
        """获取网银支付URL"""
        data = [
            '',
            ('p0_Cmd', 'Buy'),
            ('p1_MerId', YEEPAY_PC_MERID),
            ('p2_Order', transid),
            ('p3_Amt', str(total_fee)),
            ('p4_Cur', 'CNY'),
            ('p5_Pid', title),
            ('p6_Pcat', ''),
            ('p7_Pdesc', ''),
            ('p8_Url', YEEPAY_NETBANK_CALLBACK_URL),
            ('p9_SAF', '1'),
            ('pa_MP', 'None'),
            ('pd_FrpId', bank_id),
            ('pr_NeedResponse', '1'),
        ]
        origin_str = reduce(lambda x, y: '%s%s' % (x, y[1]), data)
        mac = hmac.new(YEEPAY_PC_MERKEY)
        mac.update(origin_str)
        hash_key = mac.hexdigest()
        url = ''.join([YEEPAY_PC_REQ_URL, reduce(lambda x, y: '%s&%s=%s' % (x, y[0], y[1]), data).lstrip('&'), '&hmac=', hash_key])
        return url
