#encoding=utf8
import hashlib
from config import BFB_KEY

class Baifubao_Service(object):
    def get_bfb_pay_signature(self, params):
        '''
        获取签名
        '''
        string = '&'.join(['%s=%s' % (key, params[key]) for key in sorted(params)])
        string += ('&key=%s' % BFB_KEY)
        signature = hashlib.md5(string).hexdigest().upper()
        return signature
