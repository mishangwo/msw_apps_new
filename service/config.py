# coding: utf-8
'''
一些配置相关的参数
'''
# 迷上我服务号相关的微信配置
MSW_SERVICE_WX_APP_ID = 'wx0f4e9f5cbf46a596'
MSW_SERVICE_WX_APP_SECRET = '7b8f56faeb27962857533a739b30eb3e'
MSW_SERVICE_WX_MCH_ID = '1459865502'
MSW_SERVICE_WX_PAY_API_KEY = '6452dc599b188ba87b0e10b82c2d701c'

MSW_SERVICE_WX_CREATE_ORDER_URL = 'https://api.mch.weixin.qq.com/pay/unifiedorder'
MSW_SERVICE_WX_TRADE_TYPE_JSAPI = 'JSAPI'
MSW_SERVICE_WX_TRADE_TYPE_NATIVE = 'NATIVE'

MSW_SERVICE_WX_PAY_NOTIFY_URL = 'https://apps.mishangwo.com/wxpay/notify'

# 迷上我公众号相关的微信配置
WX_APP_ID = 'wxf63bd1d65d623115'
WX_APP_SECRET = '3c5f9f99d4bbae02a663b4adcc370b64'
WX_MCH_ID = '1230909702'
WX_PAY_API_KEY = 'yahuahua515jc1305xinjianglu1305m'

WX_CREATE_ORDER_URL = 'https://api.mch.weixin.qq.com/pay/unifiedorder'
WX_TRADE_TYPE_JSAPI = 'JSAPI'
WX_TRADE_TYPE_NATIVE = 'NATIVE'

WX_PAY_NOTIFY_URL = 'https://apps.mishangwo.com/wxpay/notify'

MONGO_DB_HOST = 'msw_mongo01'
MONGO_DB_PORT = 27017
MONGO_DB_NAME = 'msw'

# Drupal
DP_CREATE_ORDER_URL = 'https://mobile-backend.mishangwo.com/rest/order.json'
DP_API_KEY = 'FCpejTaz6B6XGvzjBARUdavvqwR8VVvs'
DP_GET_CSRF_TOKEN_URL = 'https://mobile-backend.mishangwo.com/services/session/token'

# SKU
SKU_INFO = {
    'T1': {
        'price': 299 * 100,  # 分为单位
        # 'price': 1,  # 分为单位
        'body': '女人心30套餐'
    },
    'T2': {
        'price': 398 * 100,  # 分为单位
        # 'price': 1,  # 分为单位
        'body': '女人心45套餐'
    },
    'T1_4': {
        'price': 699 * 100,  # 分为单位
        # 'price': 1,  # 分为单位
        'body': '女人心30套餐'
    },
    'T2_4': {
        'price': 798 * 100,  # 分为单位
        # 'price': 1,  # 分为单位
        'body': '女人心45套餐'
    },
    'T1_6': {
        'price': 399 * 100,  # 分为单位
        # 'price': 1,  # 分为单位
        'body': '女人心30套餐'
    },
    'T2_6': {
        'price': 498 * 100,  # 分为单位
        # 'price': 1,  # 分为单位
        'body': '女人心45套餐'
    },
    # 以下为测试SKU
    'GAOSHOU_T1': {
        'price': 1,  # 分为单位
        # 'price': 1,  # 分为单位
        'body': '高手T1'
    },
    'GAOSHOU_T2': {
        'price': 2,  # 分为单位
        # 'price': 1,  # 分为单位
        'body': '高手T2'
    },
    'GAOSHOU_T3': {
        'price': 3,  # 分为单位
        # 'price': 1,  # 分为单位
        'body': '高手T3'
    },
    'BOOK_T1': {
        'price': 1,  # 分为单位
        # 'price': 1,  # 分为单位
        'body': '迷上我核心教程T1'
    },
    'BOOK_T2': {
        'price': 2,  # 分为单位
        # 'price': 1,  # 分为单位
        'body': '迷上我核心教程T2'
    },
    'TOUTING_T1': {
        'price': 1,  # 分为单位
        # 'price': 1,  # 分为单位
        'body': '偷听女人心教程T1'
    },
    'TOUTING_T2': {
        'price': 2,  # 分为单位
        # 'price': 1,  # 分为单位
        'body': '偷听女人心教程T2'
    },
    # 以下为正式SKU
    'G1': {
        'price': 23964,  # 分为单位
        'body': '高手支招24期'
    },
    'G2': {
        'price': 39985,  # 分为单位
        'body': '高手支招48期'
    },
    'G3': {
        'price': 49985,  # 分为单位
        # 'price': 100,  # 分为单位
        'body': '高手支招72期'
    },
    'MS': {
        'price': 4997,  # 分为单位
        'body': '迷上我核心教程'
    },
    'MK': {
        'price': 9854,  # 分为单位
        # 'price': 150,  # 分为单位
        'body': '迷上我核心教程+快速吸引套餐'
    },
    'QT': {
        'price': 5000,  # 分为单位
        # 'price': 150,  # 分为单位
        'body': '强推—推进关系'
    },
    'QG': {
        'price': 8900,  # 分为单位
        # 'price': 150,  # 分为单位
        'body': '强推+高手支招'
    },
    'KS': {
        'price': 5900,  # 分为单位
        # 'price': 150,  # 分为单位
        'body': '快速吸引'
    },
    'KQ': {
        'price': 9800,  # 分为单位
        # 'price': 150,  # 分为单位
        'body': '快速吸引+强推'
    },
}

# ORDER STATUS


class ORDER_STATUS(object):
    NEW_CREATED = 1
    CREATE_IN_WEIXIN = 2
    WEIXIN_PAID_SUCCEED = 3
    ALIPAY_SUCCEED = 4
    YEEPAY_SUCCEED = 5

# BaiFuBao
BFB_URL = 'https://www.baifubao.com/api/0/pay/0/wapdirect'
BFB_SERVICE_CODE_PAY = '1'  # 1 for pay request
BFB_SP_NO = '1000008070'
BFB_KEY = 'fzp7fGZHAXGAYaAzja3ufmcJKEF4BgiR'
BFB_SIGN_METHOD = '1'  # 1 for MD5
BFB_CURRENCY = '1'  # 1 for RMB
BFB_RETURN_URL = 'https://apps.mishangwo.com/bfbpay/return_url'
BFB_PAGE_URL = 'https://apps.mishangwo.com/bfbpay/page_url'
BFB_PAY_TYPE = '2'  # 2 for 网银支付
BFB_INPUT_CHARSET = '1'  # 1 for GBK
BFB_API_VERSION = '2'  # hardcode

# ALIPAY
ALIPAY_PARTNER = '2088701461748286'
ALIPAY_KEY = '5s3qgsxoaqlzjbj9h4wo9ypoq8hdollb'
ALIPAY_SIGN_TYPE = 'MD5'
ALIPAY_INPUT_CHARSET = 'utf-8'
ALIPAY_URL = 'https://wappaygw.alipay.com/service/rest.htm'
ALIPAY_SERVICE_CREATE = 'alipay.wap.trade.create.direct'
ALIPAY_SERVICE_PAY = 'alipay.wap.auth.authAndExecute'
ALIPAY_FORMAT = 'xml'
ALIPAY_VERSION = '2.0'
ALIPAY_SEC_ID = 'MD5'
ALIPAY_SELLER_ACCOUNT_NAME = 'order@mishangwo.com'
ALIPAY_CALLBACK_URL = 'https://apps.mishangwo.com/alipay/callback'
ALIPAY_NOTIFY_URL = 'https://apps.mishangwo.com/alipay/notify'

# YEEPAY Mobile
YEEPAY_MERID = '10012431459'
YEEPAY_TEST_MERID = 'YB01000000144'
YEEPAY_TEST_URL = 'http://mobiletest.yeepay.com'
YEEPAY_URL = 'https://ok.yeepay.com'
YEEPAY_CALLBACK_URL = 'https://apps.mishangwo.com/yeepay/callback'
YEEPAY_FCALLBACK_URL = 'https://apps.mishangwo.com/yeepay/fcallback'
YEEPAY_NETBANK_CALLBACK_URL = 'https://apps.mishangwo.com/yeepay/netbank/notify'
YEEPAY_ORDER_EXPIRE_DATE = 60  # 订单有效期，单位分钟
YEEPAY_CURRENCY = 156  # 币种，156表示
YEEPAY_PRODUCT_CATELOG = '1'  # 商品品类，1代表虚拟物品
YEEPAY_IDENTITY_TYPE = 3  # 用户标示类型，3为Email
YEEPAY_PAYTYPE = '1|2'  # 支付类型。储蓄卡和信用卡
YEEPAY_TERMINAL_TYPE = 3  # 终端标示类型
YEEPAY_ENCRYPT_KEY = '1234567890123456'
YEEPAY_MOBILE_MERKEY = 'yIQ2557d18X37F7821sX0p233H904D952625m9m0r91z6c53ldcn8y0pg60v'

# YEEPAY PC
YEEPAY_PC_MERID = '10000900619'
YEEPAY_PC_REQ_URL = 'https://www.yeepay.com/app-merchant-proxy/node?'
YEEPAY_PC_MERKEY = 'oKG625y7629mV557ydo72K3Z18V58NwW7Si68ZrDRad7r2c8714mo8a3QH3p'

# ALIPAY_v2
ALIPAY_V2_PARTNER = '2088701461748286'
ALIPAY_V2_SELLER_ID = '2088701461748286'
ALIPAY_V2_KEY = '5s3qgsxoaqlzjbj9h4wo9ypoq8hdollb'
ALIPAY_V2_INPUT_CHARSET = 'utf-8'
ALIPAY_V2_SERVICE = 'alipay.wap.create.direct.pay.by.user'
ALIPAY_V2_SERVICE_PC = 'create_direct_pay_by_user'
ALIPAY_V2_SIGN_TYPE = 'MD5'
ALIPAY_V2_NOTIFY_URL = 'https://apps.mishangwo.com/alipay/notify_v2'
ALIPAY_V2_RETURN_URL = 'https://apps.mishangwo.com/alipay/return_url'
ALIPAY_V2_PAYMENT_TYPE = 1
ALIPAY_V2_GATEWAY = 'https://mapi.alipay.com/gateway.do'


# GATEWAY相关的一些配置参数
GATEWAY_PAY_TYPE_ALIPAY = 'alipay'
GATEWAY_PAY_TYPE_YEEPAY = 'yeepay'
GATEWAY_PAY_TYPE_WEIXIN = 'weixin'
GATEWAY_VALIDATE_PAY_TYPE = [
    GATEWAY_PAY_TYPE_ALIPAY,
    GATEWAY_PAY_TYPE_YEEPAY,
    GATEWAY_PAY_TYPE_WEIXIN
]
GATEWAY_PRODUCT_NAME_GAOSHOU = 'gaoshou'
GATEWAY_PRODUCT_NAME_GAOSHOU_TEST = 'gaoshou_test_920c023032aacb0d019637b4d5ee331f'
GATEWAY_PRODUCT_NAME_BOOK = 'book'
GATEWAY_PRODUCT_NAME_TOUTING = 'touting'
GATEWAY_PRODUCT_NAME_QIANGTUI = 'qiangtui'
GATEWAY_PRODUCT_NAME_KSXY = 'ksxy'


# OSS相关的服务
OSS_ACCESS_KEY_ID = 'X8tS6xWyf7xWYE6e'
OSS_ACCESS_KEY_SECRET = 'FjHYqUkrfRoB54fB8H2CU9NWhYCD85'
OSS_ENDPOINT_INT = 'oss-cn-hangzhou-internal.aliyuncs.com'
OSS_ENDPOINT_EXT = 'https://oss.mishangwo.com/'
OSS_BUCKET = 'weixinpay-qrcode'


# 活动报名配置
ACT_INFO = {
    'SDWH': {
        'price':  500 * 100,  # 分为单位
        'body': '挽回她实地培训课程'
    }
}

ALIPAY_ACTIVITIES_CALLBACK_URL = 'https://apps.mishangwo.com/activities/alipay/callback'
ALIPAY_ACTIVITIES_NOTIFY_URL = 'https://apps.mishangwo.com/activities/alipay/notify'
YEEPAY_ACTIVITIES_CALLBACK_URL = 'https://apps.mishangwo.com/activities/yeepay/callback'
YEEPAY_ACTIVITIES_FCALLBACK_URL = 'https://apps.mishangwo.com/activities/yeepay/fcallback'
WX_PAY_ACTIVITIES_NOTIFY_URL = 'https://apps.mishangwo.com/activities/wxpay/notify'
