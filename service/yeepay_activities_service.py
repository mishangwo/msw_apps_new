#encoding=utf8
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import time, urllib
from config import YEEPAY_TEST_MERID, YEEPAY_TEST_URL, YEEPAY_ACTIVITIES_CALLBACK_URL, YEEPAY_URL,\
        YEEPAY_ACTIVITIES_FCALLBACK_URL, YEEPAY_ORDER_EXPIRE_DATE, YEEPAY_CURRENCY, YEEPAY_PRODUCT_CATELOG,\
        YEEPAY_IDENTITY_TYPE, YEEPAY_PAYTYPE, YEEPAY_TERMINAL_TYPE, YEEPAY_ENCRYPT_KEY, YEEPAY_MERID

import os
try:
    import Crypto
except Exception:
    import crypto, sys
    sys.modules['Crypto'] = crypto


from pymongo_conn import get_mongodb_conn
from Crypto.PublicKey import RSA
from Crypto import Random

from Crypto.Cipher import PKCS1_v1_5, AES
from Crypto.Hash import SHA
from Crypto.Signature import PKCS1_v1_5 as pk
import json, base64

# Test
#publickey = RSA.importKey(open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'key', 'rsa_public_key144.pem'), 'r').read())
#privatekey = RSA.importKey(open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'key', 'pkcs8_rsa_private_key144.pem'), 'r').read())

# Production
publickey = RSA.importKey(base64.b64decode(open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'key', 'msw_yeepay_public_key_from_yp.pem'), 'r').read()))
privatekey = RSA.importKey(base64.b64decode(open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'key', 'msw_yeepay_private_key.pem'), 'r').read()))

class Yeepay_Activities_Service(object):
    def __init__(self):
        self.mongo = get_mongodb_conn()
        Random.atfork()

    @staticmethod
    def _depkcs7padding(data):
        """
        反对齐
        """
        newdata = ''
        for c in data:
            if ord(c) > AES.block_size:
                newdata+=c
        return newdata

    @staticmethod
    def _pkcs7padding(data):
        """
        对齐块
        size 16
        999999999=>9999999997777777
        """
        size = AES.block_size
        count = size - len(data) % size
        if count:
            data += (chr(count)*count)
        return data

    '''
    aes加密base64编码
    '''
    def aes_base64_encrypt(self, data, key):
        """
        @summary:
            1. pkcs7padding
            2. aes encrypt
            3. base64 encrypt
        @return:
            string
        """
        cipher = AES.new(key)
        return base64.b64encode(cipher.encrypt(self._pkcs7padding(data)))

    def base64_aes_decrypt(self, data, key):
        """
        1. base64 decode
        2. aes decode
        3. dpkcs7padding
        """
        cipher = AES.new(key)
        return self._depkcs7padding(cipher.decrypt(base64.b64decode(data)))

    '''
    rsa加密
    '''
    def rsa_base64_encrypt(self, data, key):
        '''
        1. rsa encrypt
        2. base64 encrypt
        '''
        cipher = PKCS1_v1_5.new(key)
        return base64.b64encode(cipher.encrypt(data))

    def rsa_base64_decrypt(self, data, key):
        '''
        1. base64 decrypt
        2. rsa decrypt
        示例代码
        key = RSA.importKey(open('privkey.der').read())

        dsize = SHA.digest_size
        sentinel = Random.new().read(15+dsize)      # Let's assume that average data length is 15

        cipher = PKCS1_v1_5.new(key)
        message = cipher.decrypt(ciphertext, sentinel

        digest = SHA.new(message[:-dsize]).digest()
        if digest==message[-dsize:]:                # Note how we DO NOT look for the sentinel
            print "Encryption was correct."
        else:
            print "Encryption was not correct."
        '''
        cipher = PKCS1_v1_5.new(key)
        return cipher.decrypt(base64.b64decode(data), Random.new().read(15+SHA.digest_size))

    '''
    对返回结果进行解密后输出
    '''
    def result_decrypt(self, kdata, kencryptkey):
        '''
        1、返回的结果json传给data和encryptkey两部分，都为加密后的
        2、用商户私钥对encryptkey进行RSA解密，生成解密后的encryptkey。参考方法：rsa_base64_decrypt
        3、用解密后的encryptkey对data进行AES解密。参考方法：base64_aes_decrypt
        '''
        cryptkey = self.rsa_base64_decrypt(kencryptkey, privatekey)
        print '解密后的encryptkey=' + cryptkey
        rdata = self.base64_aes_decrypt(kdata, cryptkey)
        print '解密后的data=' + rdata
        return rdata

    '''
    RSA签名
    '''
    def sign(self, signdata):
        '''
        @param signdata: 需要签名的字符串
        '''
        h = SHA.new(signdata)
        signer = pk.new(privatekey)
        signn = signer.sign(h)
        signn = base64.b64encode(signn)
        return signn

    def sort(self, mes):
        '''
        作用类似与java的treemap,
        取出key值,按照字母排序后将value拼接起来
        返回字符串
        '''
        _par = []

        keys = mes.keys()
        keys.sort()
        for v in keys:
            _par.append(str(mes[v]))
        sep=''
        message = sep.join(_par)
        return message

    '''
    请求接口前的加密过程
    '''
    def requestprocess(self, mesdata):
        '''
        加密过程：
        1、将需要的参数mes取出key排序后取出value拼成字符串signdata
        2、用signdata对商户私钥进行rsa签名，生成签名signn，并转base64格式
        3、将签名signn插入到mesdata的最后生成新的data
        4、用encryptkey16位常量对data进行AES加密后转BASE64,生成机密后的data
        5、用易宝公钥publickey对encryptkey16位常量进行RSA加密BASE64编码，生成加密后的encryptkey
        '''
        signdata = self.sort(mesdata)
        #print '需要签名的排序后的字符串为：'+signdata
        signn = self.sign(signdata)


        mesdata['sign'] = signn
        data = self.aes_base64_encrypt(json.dumps(mesdata), YEEPAY_ENCRYPT_KEY)

        values={}
        #values['merchantaccount'] = YEEPAY_TEST_MERID
        values['merchantaccount'] = YEEPAY_MERID
        values['data'] = data
        values['encryptkey'] = self.rsa_base64_encrypt(YEEPAY_ENCRYPT_KEY, publickey)
        return values

    def get_yeepay_execute_url(self, otn, amount, product_name, ua, userip, email, platform='mobile'):
        mesdata = {
                    #"merchantaccount": YEEPAY_TEST_MERID,
                    "merchantaccount": YEEPAY_MERID,
                    "orderid": otn,
                    "orderexpdate": YEEPAY_ORDER_EXPIRE_DATE,
                    "transtime": int(time.time()),
                    "currency": YEEPAY_CURRENCY,
                    "amount": amount,
                    "productcatalog": YEEPAY_PRODUCT_CATELOG,
                    "userua": ua,
                    "productname": product_name,
                    "productdesc": '',
                    "userip": userip,
                    "identityid": email,
                    "identitytype": YEEPAY_IDENTITY_TYPE,
                    "callbackurl": YEEPAY_ACTIVITIES_CALLBACK_URL,
                    "fcallbackurl": YEEPAY_ACTIVITIES_FCALLBACK_URL,
                    "paytypes": YEEPAY_PAYTYPE,
                    "terminaltype": YEEPAY_TERMINAL_TYPE,
                    "terminalid": email
                }

        values = self.requestprocess(mesdata)
        url = YEEPAY_URL + '/paymobile/api/pay/request'
        #url = YEEPAY_TEST_URL + '/paymobile/api/pay/request'
        #if platform == 'mobile':
            #url = YEEPAY_URL + '/paymobile/api/pay/request'
        #else:
            #url = YEEPAY_URL + '/payweb/api/pay/request'
        execute_url = url + "?" + urllib.urlencode(values)
        return execute_url
