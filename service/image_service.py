# coding: utf-8

import qrcode
import hashlib
import time
import random
import os
import oss2

from .config import (
    OSS_ACCESS_KEY_ID,
    OSS_ACCESS_KEY_SECRET,
    OSS_ENDPOINT_INT,
    OSS_BUCKET
)

PATH = '/'.join(os.path.abspath(__file__).split('/')[:-2])
EXT = '.png'

oss_auth = oss2.Auth(OSS_ACCESS_KEY_ID, OSS_ACCESS_KEY_SECRET)
_bucket = oss2.Bucket(oss_auth, OSS_ENDPOINT_INT, OSS_BUCKET)


class ImageService(object):
    @classmethod
    def get_weixin_qrcode_image_url(cls, content):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_Q,
            box_size=5,
            border=1
        )
        qr.add_data(content)
        img = qr.make_image()
        # save to disk
        filename = hashlib.md5('%s%d%d' % (content, int(time.time()), random.randint(1, 10000))).hexdigest() + EXT
        filepath = '%s/static/weixin_qrcode/%s' % (PATH, filename)
        img.save(filepath)
        cls._upload_to_oss(filepath, filename)
        return filename

    @classmethod
    def _upload_to_oss(cls, filepath, filename):
        _bucket.put_object_from_file(filename, filepath)
