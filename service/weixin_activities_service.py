# coding: utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import time
import datetime
import random
import string
import hashlib
import requests
from config import (
    WX_APP_ID,
    WX_APP_SECRET,
    WX_MCH_ID,
    WX_PAY_API_KEY,
    WX_CREATE_ORDER_URL,
    WX_TRADE_TYPE_JSAPI,
    WX_TRADE_TYPE_NATIVE,
    WX_PAY_NOTIFY_URL,
    MSW_SERVICE_WX_APP_ID,
    MSW_SERVICE_WX_APP_SECRET,
    MSW_SERVICE_WX_MCH_ID,
    MSW_SERVICE_WX_PAY_API_KEY,
    MSW_SERVICE_WX_CREATE_ORDER_URL,
    MSW_SERVICE_WX_TRADE_TYPE_JSAPI,
    MSW_SERVICE_WX_TRADE_TYPE_NATIVE,
    MSW_SERVICE_WX_PAY_NOTIFY_URL,
    SKU_INFO,
    ORDER_STATUS,
    ACT_INFO,
    WX_PAY_ACTIVITIES_NOTIFY_URL
)
from pymongo_conn import get_mongodb_conn
import xml.etree.ElementTree as ET


class Weixin_Activities_Service(object):
    def __init__(self):
        self.mongo = get_mongodb_conn()

    def _create_nonce_str(self):
        return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(15))

    def _create_timestamp(self):
        return int(time.time())

    def _get_access_token(self):
        '''
        获取微信access_token, 优先从数据库中取，如果没有，或者已过期，则重新从微信服务器获取
        '''
        token = self.mongo.weixin_token_activities.find_one()
        now = int(time.time())
        if token and token['expires_at'] > now:
            return token['token']
        else:
            self.mongo.weixin_token_activities.remove({})
            token, expires_in = self._get_access_token_from_weixin()
            data = {
                'token': token,
                'expires_at': (now + expires_in - 300)  # 预留5分钟的余量
            }
            self.mongo.weixin_token_activities.insert(data)
            return token

    def _get_access_token_from_weixin(self):
        '''
        通过请求微信服务器获取access_token
        '''
        url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s' % (MSW_SERVICE_WX_APP_ID, MSW_SERVICE_WX_APP_SECRET)
        resp = requests.get(url, verify=True)
        json_res = resp.json()
        access_token = json_res['access_token']
        expires_in = int(json_res['expires_in'])
        return access_token, expires_in


    def _get_ticket(self):
        '''
        获取ticket，优先从数据库中取，如果没有，或者已过期，则重新从微信服务器获取
        '''
        now = int(time.time())
        ticket = self.mongo.js_ticket_activities.find_one()
        if ticket and ticket['expires_at'] > now:
            return ticket['ticket']
        else:
            self.mongo.js_ticket_activities.remove({})
            ticket, expires_in = self._get_ticket_from_weixin()
            data = {
                'ticket': ticket,
                'expires_at': (now + expires_in - 300)  # 预留5分钟余量
            }
            self.mongo.js_ticket_activities.insert(data)
            return ticket



    def _get_ticket_from_weixin(self):
        '''
        从微信服务器获取ticket
        '''
        access_token = self._get_access_token()

        url = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%s&type=jsapi' % access_token
        resp = requests.get(url, verify=True)
        json_res = resp.json()
        try:
            ticket = json_res['ticket']
            expires_in = int(json_res['expires_in'])
        except Exception, e:
            print "Failed getting ticket. (%s)" % str(e)
            ticket = ''
            expires_in = 0

        return ticket, expires_in



    def get_oauth_access_token_and_openid(self, code):
        '''
        获取网页授权access_token和openid
        '''
        url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code' % (MSW_SERVICE_WX_APP_ID, MSW_SERVICE_WX_APP_SECRET, code)
        resp = requests.get(url, verify=True)
        json_res = resp.json()
        access_token = json_res['access_token'] if 'access_token' in json_res else ''
        openid = json_res['openid'] if 'openid' in json_res else ''
        return access_token, openid



    def _create_wx_pay_signature(self, params):
        '''
        获取生成预付订单时的签名
        '''
        string = '&'.join(['%s=%s' % (key, params[key]) for key in sorted(params)])
        string += ('&key=%s' % MSW_SERVICE_WX_PAY_API_KEY)
        signature = hashlib.md5(string).hexdigest().upper()
        return signature



    def get_jssdk_info(self, url):
        '''
        获取初始化JSSDK时需要用到的信息
        '''
        params = {
            'noncestr': self._create_nonce_str(),
            'url': url,
            'jsapi_ticket': self._get_ticket(),
            'timestamp': str(int(time.time()))
        }
        string = '&'.join(['%s=%s' % (key.lower(), params[key]) for key in sorted(params)])
        signature = hashlib.sha1(string).hexdigest()
        params['signature'] = signature
        params['app_id'] = MSW_SERVICE_WX_APP_ID
        return params



    def create_prepay_order(self, email, username, telephone, openid, sku, spbill_create_ip):
        '''
        生成微信预付订单
        '''
        sku_info = ACT_INFO[sku]

        _now = datetime.datetime.now()
        random_str = hashlib.md5(str(time.time())).hexdigest()[:4].upper()
        out_trade_no = sku + datetime.datetime.strftime(_now, '%Y%m%d%H%M%S') + random_str

        order_data = {
            'appid': MSW_SERVICE_WX_APP_ID,
            'mch_id': MSW_SERVICE_WX_MCH_ID,
            'nonce_str': self._create_nonce_str(),
            'body': sku_info['body'],
            'openid': openid,
            'out_trade_no': out_trade_no,
            'total_fee': sku_info['price'],
            'spbill_create_ip': spbill_create_ip,
            'notify_url': WX_PAY_ACTIVITIES_NOTIFY_URL,
            'trade_type': MSW_SERVICE_WX_TRADE_TYPE_JSAPI
        }
        order_data['sign'] = self._create_wx_pay_signature(order_data)

        # 以上是需要POST到微信服务器的数据，不能多也不能少！
        # 在本地创建订单时，除了以上数据，还需要一些其他附加数据
        order_data_to_wx = order_data.copy()

        order_data['_type'] = 'weixin'
        order_data['status'] = ORDER_STATUS.NEW_CREATED
        order_data['created_at'] = datetime.datetime.now()
        order_data['email'] = email
        order_data['sku'] = sku
        order_data['username'] = username
        order_data['telephone'] = telephone

        self.mongo.activities.insert(order_data)

        # 调用微信接口，创建订单
        params = Weixin_Activities_Service.dict_to_xml(order_data_to_wx)
        resp = requests.post(MSW_SERVICE_WX_CREATE_ORDER_URL, data=params, verify=True)

        '''
        成功时返回的数据
        <xml>
            <return_code><![CDATA[SUCCESS]]></return_code>
            <return_msg><![CDATA[OK]]></return_msg>
            <appid><![CDATA[wxf63bd1d65d623115]]></appid>
            <mch_id><![CDATA[1230909702]]></mch_id>
            <nonce_str><![CDATA[bc2vN01eBg1f6UMB]]></nonce_str>
            <sign><![CDATA[832F3C7463E8BAA410FB3A6A31957A24]]></sign>
            <result_code><![CDATA[SUCCESS]]></result_code>
            <prepay_id><![CDATA[wx20150304020649c2ff0fa8030577588606]]></prepay_id>
            <trade_type><![CDATA[JSAPI]]></trade_type>
        </xml>
        '''
        resp = Weixin_Activities_Service.xml_to_dict(resp.content)
        succeed = False
        if resp['return_code'] == 'SUCCESS':  # 通信成功
            if resp['result_code'] == 'SUCCESS':  # 订单创建成功
                order_data['prepay_id'] = resp['prepay_id']
                order_data['status'] = ORDER_STATUS.CREATE_IN_WEIXIN
                succeed = True
                # 更新信息到mongo
                self.mongo.activities.update({'_id': order_data['_id']}, {"$set": {'prepay_id': order_data['prepay_id'], 'status': 2}})

        # 生成payinfo，该payinfo将传给前端以发起支付请求
        payinfo = {}
        if succeed:
            payinfo['appId'] = MSW_SERVICE_WX_APP_ID
            payinfo['timeStamp'] = int(time.time())
            payinfo['nonceStr'] = self._create_nonce_str()
            payinfo['package'] = 'prepay_id=%s' % order_data['prepay_id']
            payinfo['signType'] = 'MD5'
            payinfo['paySign'] = self._create_wx_pay_signature(payinfo)
            payinfo['otn'] = out_trade_no

        return succeed, payinfo

    @staticmethod
    def dict_to_xml(dic):
        xml = ["<xml>"]
        for k, v in dic.iteritems():
            v = str(v)
            if v.isdigit():
                xml.append("<{0}>{1}</{0}>".format(k, v))
            else:
                xml.append("<{0}><![CDATA[{1}]]></{0}>".format(k, v))
        xml.append("</xml>")
        return "".join(xml)

    @staticmethod
    def xml_to_dict(xml):
        dic = {}
        root = ET.fromstring(xml)
        for child in root:
            value = child.text
            dic[child.tag] = value
        return dic
