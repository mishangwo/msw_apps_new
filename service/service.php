<?php
define('MSW_API_KEY', 'FCpejTaz6B6XGvzjBARUdavvqwR8VVvs');
define('CSRF_TOKEN_URL', 'http://mobile-backend.mishangwo.com/services/session/token');
// Post, Put, Delete等动作需获取X-CSRF-Token并在请求时附加

/**
 * Get all products
 */
function get_all_products() {
    //$service_url = 'http://msw.drucloud.com/rest/product.json';
    $service_url = 'http://mobile-backend.mishangwo.com/rest/product.json';
    $curl = curl_init($service_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    //curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Api-Key: ' . MSW_API_KEY
    ));
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}

function user_login() {
    $service_url = 'http://mobile-backend.mishangwo.com/rest/user/login.json';
    $data = array(
      'username' => 'keithyau',
      'password' => 'thomas',
    );
    $post_data = http_build_query($data, '', '&');
    $curl = curl_init($service_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    //curl_setopt($curl, CURLOPT_COOKIE, "$session_cookie");
    curl_setopt($curl, CURLOPT_POST, TRUE);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Api-Key: ' . MSW_API_KEY
    ));
    // Output to command line.
    //curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
    $response = curl_exec($curl);
    $status_code = intval(curl_getinfo($curl, CURLINFO_HTTP_CODE));
    curl_close($curl);
    if ($status_code != 200)
        return FALSE;
    return json_decode($response, TRUE);
}

function get_csrf_token($cookie) {
    //$service_url = 'http://msw.drucloud.com/rest/order.json';
    $curl = curl_init(CSRF_TOKEN_URL);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    //curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
    curl_setopt($curl, CURLOPT_COOKIE, $cookie);
    // Output to command line.
    $response = curl_exec($curl);
    $status_code = intval(curl_getinfo($curl, CURLINFO_HTTP_CODE));
    curl_close($curl);
    if ($status_code != 200)
        return FALSE;
    return trim($response);
}

function create_order($order_info) {
    //$service_url = 'http://msw.drucloud.com/rest/order.json';
    $service_url = 'http://mobile-backend.mishangwo.com/rest/order.json';
    $post_data = http_build_query($order_info, '', '&');
    $csrf_token = file_get_contents(CSRF_TOKEN_URL); // add try catch by yourself
    $curl = curl_init($service_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_POST, TRUE);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Api-Key: ' . MSW_API_KEY,
      'X-CSRF-Token: ' . $csrf_token
    ));
    // Output to command line.
    $response = curl_exec($curl);
    $status_code = intval(curl_getinfo($curl, CURLINFO_HTTP_CODE));
    curl_close($curl);
    if ($status_code != 200)
        return FALSE;
    return $response;
}

function get_order_info_by_order_id($order_id) {
    /* order info looks like below
     * {
     *    "oid": "16",
     *    "uid": "0",
     *    "order_key": "U7N8B6C8",
     *    "order_id": "de3da08ea11c19d29a8b57fdab9ff80f",
     *    "status": "pending",
     *    "gateway": "AliPay",
     *    "amount": 0.01,
     *    "total": 0.01,
     *    "currency": "CNY",
     *    "email": "jude@qq.com",
     *    "products": [
     *       {
     *          "opid": "9",
     *          "oid": "16",
     *          "name": "核心教程套餐A",
     *          "module": "msw_product",
     *          "type": "group",
     *          "qty": "1",
     *          "price": "0.01000",
     *          "sku": "MSW50",
     *          "id": "10",
     *          "data": null
     *       }
     *    ],
     *    "payments": [
     *       {
     *          "opid": "11",
     *          "oid": "16",
     *          "gateway": "AliPay",
     *          "channel": "DirectPay",
     *          "type": "Cart",
     *          "amount": "0.01",
     *          "currency": "CNY",
     *          "ip_address": "58.34.42.42",
     *          "extra_data": "",
     *          "transaction": "Fn9m5gebFBQl3FfzzScF85YSG9E19CH6WUg20II7s-I",
     *          "created": "1387893003"
     *       }
     *    ],
     *    "history": [
     *       {
     *          "ohid": "11",
     *          "oid": "16",
     *          "uid": "0",
     *          "hidden": "1",
     *          "status": "pending",
     *          "message": "Auto create order with restful api.",
     *          "created": "1387893003"
     *       }
     *    ],
     *    "created": "1387893002",
     *    "changed": "1387893003"
     * }
     */

    $service_url = 'http://mobile-backend.mishangwo.com/rest/order/' . $order_id . '.json';
    $curl = curl_init($service_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Api-Key: ' . MSW_API_KEY
    ));
    $response = curl_exec($curl);
    $status_code = intval(curl_getinfo($curl, CURLINFO_HTTP_CODE));
    curl_close($curl);
    if ($status_code != 200)
        return FALSE;
    return json_decode($response, TRUE);
}

function update_order_info_by_order_id($order_id, $data, $cookie, $csrf_token) {
    $service_url = 'http://mobile-backend.mishangwo.com/rest/order/' . $order_id . '.json';
    $post_data = http_build_query($data, '', '&');
    $curl = curl_init($service_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
    //curl_setopt($curl, CURLOPT_POST, TRUE);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
    //curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Api-Key: ' . MSW_API_KEY,
      'X-CSRF-Token: ' . $csrf_token,
      'X-HTTP-Method-Override: PUT',
      'Cookie: ' . $cookie
    ));
    // Output to command line.
    $response = curl_exec($curl);
    $status_code = intval(curl_getinfo($curl, CURLINFO_HTTP_CODE));
    curl_close($curl);
    if ($status_code != 200)
        return FALSE;
    return TRUE;
}

function get_home_items() {
    //$service_url = 'http://msw.drucloud.com/rest/product.json';
    $service_url = 'https://mobile-backend.mishangwo.com/rest/item/iphone.json';
    $curl = curl_init($service_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    //curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Api-Key: ' . MSW_API_KEY
    ));
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}

var_dump(get_home_items());
