(function(){
	
	var $win = $(window);
	var techSide = $('#J_fixedbar'),
		techSideTop = techSide.offset().top;

	// 顶部图标
	$('.menu-list').delegate('li.menu-item', 'mouseenter mouseleave', function(e){
        var $this = $(this),
            dropbox = $this.find('div.dropbox');
        if(dropbox[0]){
            e.type === 'mouseenter' ? $this.addClass('active') : $this.removeClass('active');                  
        }
    });

    // 视频播放
    var player = new MediaElementPlayer('#player', {alwaysShowControls: true});
    $(player.$media).on('ended', function(){
        $('#' + player.id).find('.mejs-poster').fadeIn();
    });         

	// MP3 播放
    var audioSetting = {
        audioWidth: 260,
        audioHeight: 30,        
        audioVolume: 'vertical',
        features: ['playpause', 'progress','volume']
    };

    $('.audition-audio').mediaelementplayer(audioSetting);
    $('.letter-audio').mediaelementplayer($.extend({}, audioSetting, {
        audioWidth: 340       
    }));

    // 侧栏
    var scrollTimer = null;
    $win.on('scroll resize', function(e){
    	var $this = $(this);
        scrollTimer && clearTimeout(scrollTimer);
        scrollTimer = setTimeout(function(){
            if($this.scrollTop() >= techSideTop){
	            techSide.addClass('fixed');
	        }else{
	            techSide.removeClass('fixed');
	        }
        }, 10);
    });

})();