(function(){

    var utils = {
        reg: {
            mobile: /^0?(13|15|18|14|17)[0-9]{9}$/,
            email: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
        },
        valid: {
            isEmpty: function(value) {
                return $.trim(value) === '';
            },
            isEmail: function(value){
                return utils.reg.email.test(value);
            }
        }
    };

    var TIP_UNAME_NULL = '请输入昵称',
        TIP_UEMAIL_NULL = '请输入邮箱地址',
        TIP_UEMAIL_ERROR = '邮箱格式不正确';

    var $win = $(window),
        $doc = $(document),
        techSide = $('#J_tech'),
        techOffset = techSide.offset(),
        validFrom = $('#J_validform'),
        foldHandl = $('#J_fold_handl'),
        techSideHeight = techSide.innerHeight(),
        footTop = $('.m-footer').height() + parseInt($('.m-footer').css('padding-top'));

    var cellOPened = false;

    var page = {
        init: function(){
            this.checkSideFixed();
            this.bindEvent();
            this.upPlayer();
        },
        bindEvent: function(){
            var self = this;

            // 介绍文字的展开收起
            this.toggleSlide(foldHandl, $('#J_fold_panel'));
            
            // 侧栏
            var scrollTimer = null;
            $win.on('scroll resize', function(e){
                scrollTimer && clearTimeout(scrollTimer);
                scrollTimer = setTimeout(function(){
                    self.checkSideFixed();
                }, 0);
            });

            this.placeholder($('input.input'));

            // 邮箱自动补全
            $('#J_it_email,#J_tc_email,#J_dv_email').mailAutoComplete();

            // 点击获取教程
            $('.j-tech-btn').on('click', function(){
                var form = $(this).parents('.form'),
                    uname = form.find('input.uname').val(),
                    uemail = form.find('input.uemail').val();

                var errors = [];

                // 昵称为空时
                if(utils.valid.isEmpty(uname)){
                    errors.push(TIP_UNAME_NULL);
                }

                // 邮箱为空或邮箱格式不正确时
                if(utils.valid.isEmpty(uemail)){
                    errors.push(TIP_UEMAIL_NULL);
                }else if(!utils.valid.isEmail(uemail)){
                    errors.push(TIP_UEMAIL_ERROR);
                }

                if(errors.length){
                    
                    var showFaildedTip = function(){
                        validFrom.find('p.failed').text(errors.join(' / '));
                    };

                    if(form.data('v') === 1){
                        showFaildedTip();
                    }else{
                        validFrom.find('input.uname').val(uname);
                        validFrom.find('input.uemail').val(uemail);
                        showFaildedTip();
                        dialog({
                            skin: 'valid-from',
                            title: ' ',
                            content: validFrom,
                            onshow: function(){
                                // 表单input获得焦点时，隐藏错误提示
                                var current = $(this.__popup);
                                current.find('button.ui-dialog-close').text('[关闭]');
                                current.find('input').on('focus', function(){
                                    current.find('p.failed').empty();
                                });
                            }
                        }).showModal();
                    }
                    
                    return false;
                }

                // ajax提交请求
                 
            });

            $('.menu-list').delegate('li.menu-item', 'mouseenter mouseleave', function(e){
                var $this = $(this),
                    dropbox = $this.find('div.dropbox');
                if(dropbox[0]){
                    e.type === 'mouseenter' ? $this.addClass('active') : $this.removeClass('active');                  
                }

            });
        },
        checkSideFixed: function(){
            var $winHeight = $win.height(),
                needFixed = $winHeight - footTop > techSideHeight;

            if(cellOPened){
                if($win.scrollTop() >= techOffset.top){
                    techSide.addClass('fixed');
                }else{
                    techSide.removeClass('fixed');
                }
            }else if($win.scrollTop() >= techOffset.top && needFixed){
                techSide.addClass('fixed');
            }else{
                techSide.removeClass('fixed');
            }
        },
        toggleSlide: function(handle, panel){
            var toggleEnd = true;
            handle.on('click', function(){
                var $this = $(this);

                if(toggleEnd){

                    toggleEnd = false;

                    $this.toggleClass('active');

                    if($this.hasClass('active')){
                        panel.slideDown(function(){
                            toggleEnd = true;
                            cellOPened = true;
                        }); 
                    }else{
                        panel.slideUp(function(){
                            toggleEnd = true;
                            cellOPened = false;
                        });       
                    }                    
                }
            });
        },

        placeholder: function(elem){

            var supportPlaceholder = 'placeholder' in document.createElement('input');

            // 在不支持placeholder情况下，用label形式模仿
            if(!supportPlaceholder){

                elem.each(function(){
                    
                    var $this = $(this),
                        _id = '', label;

                    $this.val('');

                    if(this.id){
                        _id = this.id;
                    } else {
                        _id = '_' + new Date().getTime();
                        this.id = _id;
                    }

                    label = $('<label for="'+ _id +'" class="placeholder">'+ ($this.attr('placeholder') || '') +'</label>');

                    $this.before(label);

                    $this.on('focus blur', function(e){
                        if(e.type === 'focus') {
                            label.hide();
                        } else {
                            $.trim($(this).val()) === '' ? label.show() : label.hide();
                        }
                    });                    
                });
            }
        },        
        upPlayer: function(){
            var player = new MediaElementPlayer('#player', {alwaysShowControls: true});

            // player.play();
            
            $(player.$media).on('ended', function(){
                $('#' + player.id).find('.mejs-poster').fadeIn();
            });            
        }
    };

    $(function(){
        page.init();
    });
})();