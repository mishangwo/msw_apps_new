(function(){

    var utils = {
        reg: {
            mobile: /^0?(13|15|18|14|17)[0-9]{9}$/,
            email: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
        },
        valid: {
            isEmpty: function(value) {
                return $.trim(value) === '';
            },
            isEmail: function(value){
                return utils.reg.email.test(value);
            }
        }
    };

    var bankDialog = $('#J_pay_bank_dialog'),
        bankOther = $('#J_pay_type_other'),
        bankList = $('#J_pay_bank-list'),
        resultDialog = $('#J_pay_result_dialog'),
        paycardEl = $('#J_paycard_val');
        yeepay_kuaijie_card_no = $('#yeepay_kuaijie_card_no');


    function validMail(val){
        var error = [];
        utils.valid.isEmpty(val) && error.push('请输入邮箱地址');
        !utils.valid.isEmail(val) && error.push('邮箱格式不正确');

        return error;
    }

    function submit_pay_info(_type, email, sku, _extra_info) {
        if (_type == 'yeepay_other' || _type == 'mob' || _type == 'yeepay_kuaijie') {
            _type = 'yeepay';
        }

        var error = [];
        $.ajax({
            "async": false,
            "type": "POST",
            "url": "/gateway/api/pay/submit/pc",
            "data": {
              "pay_type": _type,
              "extra_info": _extra_info,
              "email": email,
              "sku": sku,
            },
            "error": function() {
                error.push('提交支付信息失败');
            },
            "success": function(data) {
                if (data.success){
                    window.open('https://apps.mishangwo.com/jump?url=' + data.link)
                } else {
                    error.push(data.msg)
                }
            }
        });
        return error
    }


    var pay = {
        init: function(){
            this.bindEvent();
        },
        bindEvent: function(){
            var self= this;

            // 邮箱自动补全
            $('#J_email_input').mailAutoComplete();

            //  修改邮箱
            var originEmailBlock = $('#email-orgin'),
                originEmailText = $('#email-text'),
                emailChangeTrigger = $('#J_email_change'),
                editEmailBlock = $('#email-edit'),
                editEmailInput = $('#J_email_input'),
                hidden_div_sku = $('#hidden_div_sku');

            var hasBindAutoComplete = false;

            var validEditMail = function(){
                var val = editEmailInput.val();
                var error = validMail(val);

                if(error.length){
                    self.showMessage(error[0]);
                }

                return error;
            };

            emailChangeTrigger.on('click', function(){
                originEmailBlock.hide();
                editEmailBlock.show();

                if(!hasBindAutoComplete){
                    hasBindAutoComplete = true;
                    // 邮箱自动补全
                    $('#J_email_input').mailAutoComplete();
                }
            });

            $('#J_email_ok').on('click', function(){

                var val = editEmailInput.val();
                if(validEditMail(val).length) return false;

                originEmailText.text(val);
                editEmailBlock.hide();
                originEmailBlock.show();
            });

            $('#J_email_cancel').on('click', function(){

                originEmailBlock.show();
                editEmailBlock.hide();
                editEmailInput.val(originEmailText.text());
            });

            // 支付方式切换
            this.switchable({
                trigger: '#J_pay_tabs_nav .pay-tabs-trigger',
                panel: '#J_pay_tabs_content .pay-tabs-panel',
                callback: function(info){}
            });

            // 支付平台(支付宝、微信、快捷支付)切换
            this.switchable({
                trigger: '#J_platform_nav .pay-type-item',
                panel: '#J_platform_panel .platform-panel'
            });

            // 选择列表事件
            $('.pay-type-list').delegate('li.pay-type-item', 'click', function(){
                var $this = $(this);
                if($this.hasClass('active')){
                    return false;
                }

                if($this.data('type') === 'mob'){
                    $('.mob-desc').show();
                    paycardEl.val($this.data('val'));
                }
                self.toggleSiblingClass($this, 'active');
            });

            // 选其他银行
            bankOther.on('change', function(){
                var $this = $(this);
                if($this.is(':checked')){
                    // 弹出银行列表
                    dialog({
                        skin: 'simple',
                        title: ' ',
                        width: 490,
                        content: bankDialog,
                        onshow: function(){

                            var popup = $(this.__popup);
                            popup.find('button.ui-dialog-close').text('[关闭]');
                            bankList.find('.pay-type-item').removeClass('active');

                        },
                        onclose: function(){
                            bankOther.prop('checked', false);
                            bankList.find('.pay-type-item')
                                .eq(0).trigger('click');
                        }
                    }).showModal();
                }
            });

            $('.require.u-button').on('click', function(){
                var $this = $(this);
                var type = $this.data('type');
                var email = originEmailText.text();
                var sku = hidden_div_sku.text().trim();
                var _val = '';

                if(type === 'mob'){  // 充值卡支付
                    _val = $('.pay-type-item.mob.active').data('val');
                } else if (type == 'yeepay_kuaijie') {  // Yeepay快捷支付
                    _val = 'YJZF-NET'
                } else if (type == 'yeepay') {  // Yeepay支付
                    _val = $('.pay-type-item.yeepay.active').data('val');
                } else if (type == 'alipay') {  // Yeepay支付
                    _val = ''
                } else if (type == 'yeepay_other') {  // Yeepay支付
                    _val = $('.pay-type-item.yeepay_other.active').data('val');
                } else {
                    alert("未知支付类型！");
                    return false;
                }
                var error = submit_pay_info(type, email, sku, _val);
                if (error.length) {
                    self.showMessage(error[0]);
                    return false;
                }
                self.showPayResult();
            });
        },
        showMessage: function(message){
            dialog({
                skin: 'simple',
                title: ' ',
                width: 400,
                height: 100,
                content: '<p class="failed" style="margin-top:25px;">' + message + '</p>',
                onshow: function(){
                    $(this.__popup).find('button.ui-dialog-close').text('[关闭]');
                }
            }).showModal();
        },
        switchable: function(option){
            var self= this;
            var trigger = $(option.trigger),
                panel = $(option.panel);

            trigger.on('click', function(e){

                e.preventDefault();
                var $this = $(this),
                    sbIndex = $this.siblings('.active').index(),
                    prevIndex = sbIndex > -1 ? sbIndex : 0,
                    currentIndex = $this.index();

                self.toggleSiblingClass($this, 'active');
                panel.eq(currentIndex).show().siblings().hide();

                option.callback && option.callback({
                    prevIndex: prevIndex,
                    currentIndex: currentIndex,
                    prevPanel: panel.eq(prevIndex),
                    currentPanel: panel.eq(currentIndex)
                });

            });

            trigger.eq(0).trigger('click');
        },
        toggleSiblingClass: function(elem, className){
            elem.addClass(className)
                .siblings()
                .removeClass(className);
        },
        showPayResult: function(){
            // 弹出支付是否完成或遇到问题
            dialog({
                skin: 'simple',
                title: ' ',
                width: 440,
                content: resultDialog,
                onshow: function(){

                    var popup = $(this.__popup);
                    popup.find('button.ui-dialog-close').text('[关闭]');

                },
                onclose: function(){
                    $this.prop('checked', false);
                }
            }).showModal();
        }
    };

    function get_weixin_pay_status() {
        out_trade_no = $('#hidden_div_wx_out_trade_no').text().trim();
        $.get("/gateway/api/pay/status?type=weixin&out_trade_no=" + out_trade_no, function(data) {
            if (data.success && data.paid) {
                window.location = '/gateway/pay_succeed_pc/weixin?out_trade_no=' + out_trade_no;
            }
        });
    }

    $(function(){
        pay.init();
    });

    $(function(){
        setInterval(get_weixin_pay_status, 1000);
    });

})();

