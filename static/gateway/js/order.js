(function(){

    var utils = {
        reg: {
            mobile: /^0?(13|15|18|14|17)[0-9]{9}$/,
            email: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
        },
        valid: {
            isEmpty: function(value) {
                return $.trim(value) === '';
            },
            isEmail: function(value){
                return utils.reg.email.test(value);
            }
        }
    };

    var TIP_UEMAIL_NULL = '请填写电子邮件地址！',
        TIP_UEMAIL_ERROR = '填写的电子邮件格式不正确！',
        TIP_UEMAIL_CONFRIM_NULL = '请填写确认电子邮件地址！',
        TIP_UEMAIL_CONFRIM_ERROR = '两次输入的邮件地址不匹配！';

    function placeholder (elem) {

        var supportPlaceholder = 'placeholder' in document.createElement('input');

        // 在不支持placeholder情况下，用label形式模仿
        if(!supportPlaceholder){

            elem.each(function(){

                var $this = $(this),
                    _id = '', label;

                $this.val('');

                if(this.id){
                    _id = this.id;
                } else {
                    _id = '_' + new Date().getTime();
                    this.id = _id;
                }

                label = $('<label for="'+ _id +'" class="placeholder">'+ ($this.attr('placeholder') || '') +'</label>');

                $this.before(label);

                $this.on('focus blur', function(e){
                    if(e.type === 'focus') {
                        label.hide();
                    } else {
                        $.trim($(this).val()) === '' ? label.show() : label.hide();
                    }
                });
            });
        }
    }


    $(function(){

        var mainOriginEl = $('#J_email'),
            mailConfirmEl = $('#J_email_confrim'),
            submitBtn = $('#J_submitbtn');

        // 邮箱验证
        function validMail(originValue, confirmVal){

            var error = [];
            utils.valid.isEmpty(originValue) && error.push(TIP_UEMAIL_NULL);
            !utils.valid.isEmail(originValue) && error.push(TIP_UEMAIL_ERROR);

            utils.valid.isEmpty(confirmVal) && error.push(TIP_UEMAIL_CONFRIM_NULL);
            !utils.valid.isEmail(confirmVal) && error.push(TIP_UEMAIL_ERROR);

            confirmVal !== originValue && error.push(TIP_UEMAIL_CONFRIM_ERROR);

            return error;
        }

        var discountTable = $('#J_discount_table');
        $('#discount').on('change', function(){
            $(this).is(':checked') ?
                discountTable.show() :
                discountTable.hide();
        });

        // 提交表单时验证邮箱
        $('#J_order_form').on('submit', function(e){
            var originVal = mainOriginEl.val(),
                confirmVal = mailConfirmEl.val();

            var error = validMail(originVal, confirmVal);

            if(error.length){
                dialog({
                    skin: 'simple',
                    title: ' ',
                    width: 400,
                    height: 100,
                    content: '<p class="failed" style="margin-top:25px;">' + error[0] + '</p>',
                    onshow: function(){
                        $(this.__popup).find('button.ui-dialog-close').text('[关闭]');
                    }
                }).showModal();

                return false;
            }
        });

        placeholder($('input.input'));

        // 邮箱自动补全
        $('#J_email').mailAutoComplete();

    });
})();
