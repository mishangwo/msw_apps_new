(function($) {
  $(document).ready(function() {

    // Video mobile
    $('video.mobile').each(function() {
      var player = new MediaElementPlayer(this, {alwaysShowControls: true});
      var $media = $(player.$media), mediaEl = $('#' + player.id);
      $media.on('ended', function() {
        mediaEl.find('.mejs-poster').fadeIn();
      }).on('click', function() {
        player.pause();
      }).on('pause', function() {
        mediaEl.find('.mejs-overlay-play').show();
      });
    });
  });
}(jQuery));