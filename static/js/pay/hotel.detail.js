$(function () {
	$('.header .back').click(function (e) {
		history.go(-1);
	}).attr('href', 'javascript:;')
});

// In Page Links
$('.tags a, .tabs a, .pager a').each(function () {
	$(this).data('href', this.href).attr('href', 'javascript:;');
}).click(function (e) {
	location.replace($(e.target).data('href'));
});

// Change Dates
$(function () {
	var elem = $('#dates');
	if (!elem.length) return;
	var checkIn = Calendar.parse(elem.data('checkIn'));
	var checkOut = Calendar.parse(elem.data('checkOut'));
	var onselect = function (newCheckIn, newCheckOut) {
		if (newCheckIn - checkIn || newCheckOut - checkOut) {
			var obj = {
				checkin: Calendar.format(newCheckIn),
				checkout: Calendar.format(newCheckOut) 
			};
			showSpinner(true);
			location.search = '?' + $.param(obj);
		}
	};
	var calendar = null;
	elem.on('click', 'a', function () {
		calendar || (calendar = new Calendar(onselect, window.calendarOptions));
		calendar.show();
		calendar.selectRange(checkIn, checkOut);
	});

	// package calendar
	$('.buy.out').click(function () {
		showSpinner(true);
		$.getJSON($(this).data('calendarOptions'), function (dict) {
			var calendar = new Calendar(function (cIn, cOut) {
				location.search = '?' + $.param({
					checkin: Calendar.format(cIn),
					checkout: Calendar.format(cOut)
				});
			}, dict);
			calendar.show();
		}).fail(function () {
			alert('网络请求失败，请稍后重试');
		}).always(function () {
			showSpinner(false);
		});
	});
});

function showSpinner(flag) {
	if (flag) {
		$('<div class="spinner dark"><a></a></div>').appendTo(document.body).find('a').css('opacity', 0).animate({opacity: 1}, 400);
	} else {
		$('body > div.spinner').remove();
	}
}

// Room Count and passenger Info
$(function () {
	if (!$('a.button.pay').length) {
		return false;
	}
	var check = function () {
		if (localStorage.contactPhone) {
			controller._contactPhone.text(localStorage.contactPhone).removeClass('empty');
		}
		if (localStorage.roomOptions) {
			controller._roomOptions.text(localStorage.roomOptions);
			var href = controller._roomOptions.data('href').replace(/&options=[^&]+/, '&options=' + encodeURIComponent(localStorage.roomOptions));
			controller._roomOptions.data('href', href);
			delete localStorage.roomOptions;
		}
	};
	var controller = {
		init: function () {
			this._roomCount = $('.rcount m');
			this._totalPrice = $('.ramount m');
			this._passengerBox = $('#passengers');
			this._contactPhone = $('.cell.phone a');
			this._roomOptions = $('.cell.options a');

			this._contactPhone.add(this._roomOptions).click(function () {
				controller.save();
				location.href = $(this).data('href');
			});
			$(window).on('pageshow', check);
			check();

			$('.rcount a').click(function () {
				var count = controller.getRoomCount() + ($(this).is('.add') ? 1 : -1);
				if (window.packageCount && count > window.packageCount) {
					alert("已达到库存上限");
					return;
				}
				count && controller.setRoomCount(Math.max(1, count));
			});
			
			try {
				var data = JSON.parse(localStorage.roomCountInfo);
				if (!data) {
					throw 'error';
				}
			} catch (e) {
				return;
			} finally {
				delete localStorage.roomCountInfo;
			}
			this.setRoomCount(data.roomCount);
			this._passengerBox.find('input').each(function (index) {
				$(this).val(data.passengers[index]);
			});

			window.scrollTo(0, data.scrollTop);
		},
		save: function () {
			var data = {
				roomCount: this.getRoomCount(),
				passengers: this.getPassengers(),
				scrollTop: document.body.scrollTop
			};
			localStorage.roomCountInfo = JSON.stringify(data);
			localStorage.roomOptions = this._roomOptions.text();
		},
		getRoomCount: function () {
			return parseInt(this._roomCount.text(), 10);
		},
		getPassengers: function () {
			return this._passengerBox.find('input').toArray().map(function (el) {
				return $.trim(el.value);
			});
		},
		setRoomCount: function (count) {
			this._roomCount.text(count);
			var passengers = this._passengerBox.children();
			passengers.slice(count).remove();
			for (var i = passengers.length; i < count; ++i) {
				passengers.eq(0).clone().appendTo(this._passengerBox).find('input').val('');
			}
			this._totalPrice.text(window.packagePrice * count);
			window.orderInfo.roomCount = count;
			window.orderInfo.amount = window.packagePrice * count;
		}
	};
	controller.init();

	$('.pay.next').click(function (e) {
		var passengers = controller.getPassengers();
		for (var i = 0; i < passengers.length; ++i) {
			if (!passengers[i]) {
				alert('请填写入住人姓名');
				return;
			}
		}

		$.extend(window.orderInfo, {
			contact: controller.getPassengers().join(','),
			contactPhone: localStorage.contactPhone,
			note: $.trim(controller._roomOptions.text())
		});
		if (!/^1\d{10}$/.test(orderInfo.contactPhone)) {
			alert('请填写手机号');
			return;
		}
		
		showSpinner(true);
		$.post($(this).data('href'), window.orderInfo, function (ret) {
			if (ret.error) {
				alert(ret.error);
			} else {
				location.href = ret.url;
			}
		}).always(function () {
			showSpinner(false);
		}).fail(function () {
			alert('服务出错，请稍后再试');
		});

		_trackEvent('pv_buy', window.packagePrice * passengers.length);
	});
});

// Choose Channel and Pay
$(function () {
	var orderNo = $('.payments').data('order');
	$('.payments').click('a[href]', function (e) {
		var url = $(e.target).closest('a').data	('href');
		showSpinner(true);
		$.post(url, function (ret) {
			if (ret.error) {
				alert("错误：" + ret.error);
			} else if (ret.url) {
				location.href = ret.url;
			} else if (ret.form) {
				var form = $(ret.form).hide();
				$(document.forms[form.name]).remove();
				form.appendTo('body').submit();
			} else {
				console.log(ret);
				alert('服务异常');
			}
		}).fail(function () {
			alert("服务出错，请稍后再试");
		}).always(function () {
			showSpinner(false);
		});
	});
})

// Validate Phone Number
$(function () {
	var box2 = $('#checkPhoneNumber');
	if (!box2.length) {
		return;
	}

	var input1 = box2.find('input').eq(0);
	var button1 = box2.find('a.vcode');
	var regex = /^1\d{10}$/;
	if (regex.test(localStorage.contactPhone)) {
		button1.addClass('button');
		input1.val(localStorage.contactPhone);
	}
	var unlock = 0;
	var timer = setInterval(function () {
		var delta = unlock - new Date;
		if (delta >= 500) {
			button1.removeClass('button');
			button1.text(Math.round(delta / 1000) + '秒后可重新获取');
		} else {
			button1.toggleClass('button', regex.test(input1.val()));
			if (unlock) {
				unlock = 0;
				button1.text("重新获取验证码");
			}
		}
	}, 500);
	button1.click (function () {
		if (!button1.hasClass('button')) {
			return;
		}
		button1.removeClass('button');
		unlock = new Date().getTime() + 60 * 1000;
		$.post(window.smsUrl, {
			action: 'send',
			number: input1.val()
		}, function (r) {
			console.log(r);
		});
		button2.css('display', 'block');
	});

	var input2 = box2.find('input').eq(1);
	var button2 = $('a.validate').click(function () {
		var number = input1.val();
		if (!regex.test(number)) {
			alert('请输入有效的手机号');
			return;
		}
		var code = input2.val();
		if (!/^\d+$/.test(code)) {
			alert('请输入收到的短信验证码');
			return;
		}
		showSpinner(true);
		$.post(window.smsUrl, {
			action: 'check',
			number: number,
			code: code
		}, function (r) {
			if (r && input1.val() == number) {
				localStorage.contactPhone = number;
				history.go(-1);
			} else {
			    alert('验证码错误');
			}
		}).always(function () {
			showSpinner(false);
		}).fail(function () {
			alert('网络请求失败，请稍后重试');
		});
	});
});

// Choose Options
$(function () {
	if (!$('#extra-option').length) {
		return;
	}

	$('.opts').on('click', 'a', function () {
		var el = $(this);
		el.toggleClass('on');
		if (el.hasClass('on')) {
			el.prevAll().add(el.nextAll()).removeClass('on');
		}
	});

	var match = location.search.match(/&options=([^&]+)/);
	var roomOptions = match ? decodeURIComponent(match[1]) : '';
	var saveRoomOptions = function () {
		var selected = [];
		$('.opts .on').each(function () {
			selected.push($(this).text());
		});
		var extra = $.trim($('#extra-option textarea').val().replace(/\r\n?/g, ' '));
		if (extra) {
			selected.push(extra);
		}
		localStorage.roomOptions = selected.join(' ');
	};
	$(window).on('pagehide', saveRoomOptions);

	$('.button.done').click(function () {
		('onpagehide' in window) || saveRoomOptions();
		history.go(-1);
	});
});