(function() {
	"use strict";

	HTMLElement.prototype.tap = function (callback, ignore) {
		var moved, tapCancelTime, startTime, endTime, startPoint, bind;

		tapCancelTime = 1000;
		bind = function(fn, me) { return function () { return fn.apply(me, arguments); }; };

		var is_touch_device = ('ontouchstart' in document.documentElement) || /\b(iPhone|Android)\b/.test(navigator.userAgent);
		var ev_touchstart = is_touch_device ? 'touchstart' : 'mousedown';
		var ev_touchend = is_touch_device ? 'touchend' : 'mouseup';
		var ev_touchmove = is_touch_device ? 'touchmove' : 'mousemove';
		var ev_touchcancel = is_touch_device ? 'touchcancel' : 'nothing';
		var ev_click = 'click';

		var flag = 'active';
		var klas = 'active';
        var throttle = 5;

		var hilit = function (e, t) {
			var elem = $(e.target).closest('a');
			if (t === 1) {
				elem.data(flag, 1);
			} else if (t === true) {
				elem.data(flag) == 1 && elem.addClass(klas).data(flag, 0);
			} else {
				elem.removeClass(klas).data(flag, 0);
			}
		};

        if (!ignore) {
            ignore = $.noop;
        }

		this.addEventListener(ev_click, function (e) {
			if (ignore(e)) {
				return;
			}
			e.preventDefault();
			e.stopPropagation();
		}, true);

		this.addEventListener(ev_touchstart, bind(function (e) {
			if (ignore(e) || !is_touch_device && e.which != 1) {
				return;
			}
			startTime = new Date().getTime();
			hilit(e, 1);
			setTimeout(function () {
				hilit(e, true);
			}, 100);
			moved = false;
			var touch = e.changedTouches ? e.changedTouches[0] : e;
			startPoint = {x: touch.screenX, y: touch.screenY};
		}, this));

		this.addEventListener(ev_touchmove, bind(function (e) {
			if (!startPoint || moved) {
				return;
			}
            var touch = e.changedTouches ? e.changedTouches[0] : e;
            if (Math.abs(startPoint.x - touch.screenX) < throttle && Math.abs(startPoint.y - touch.screenY) < throttle) {
                return;
            }
			hilit(e, false);
			moved = true;
		}, this));

		this.addEventListener(ev_touchend, bind(function (e) {
			if (!startPoint || ignore(e)) {
				return;
			}
			endTime = new Date().getTime();
			if(!moved && ((endTime - startTime) < tapCancelTime)) {
				e.preventDefault();
				hilit(e, true);
				setTimeout(function () {
					callback(e);
				}, 50);
				setTimeout(function () {
					hilit(e, false);
				}, 100);
			} else {
				hilit(e, false);
			}
			startPoint = false;
		}, this));

		this.addEventListener(ev_touchcancel, bind(function (e) {
			if (!startPoint || ignore(e)) {
				return;
			}
			hilit(e, false);
			startPoint = false;
		},this));
	}

}).call(this);

document.body.tap(function (e) {
	var a = $(e.target).closest('a');
	if (/^\w+?:/i.test(a.prop('href')) && !/javascript:/.test(a.prop('href'))) {
		location.href = a.prop('href');
	} else {
		a.click();
	}
}, function (e) {
	var el = e.target;
	if (/textarea|input/i.test(el.tagName) && !el.disabled) {
		return true;
	}
	while (el = el.parentNode) {
		if (el.className && el.className.indexOf('a-map') >= 0) {
			return true;
		}
	}
	return false;
});