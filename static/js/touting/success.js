(function(window, undefined){
    function bindEvent(){
        $('.j_button').on('click', function(){
            var $item = $(this),
                $siblings = $item.siblings('.j_button'),
                $img = $item.find('img'),
                $siblingsImg = $siblings.find('img'),
                src = $img.attr('src'),
                siblingsSrc = $siblingsImg.attr('src')
            src = src.replace('.png', '-active.png')
            /*
            siblingsSrc = siblingsSrc.replace('-active.png', '.png')
            $img.attr('src', src)
            $siblingsImg.attr('src', siblingsSrc)
            */
            var type = $item.attr('d-type')
            setTimeout(function(){
                download(type)
            }, 500)
        })
    }
    function isWeiXin(){
        var ua = navigator.userAgent.toLowerCase()
        if(ua.match(/MicroMessenger/i)=="micromessenger") {
            return true;
        } else {
            return false;
        }
    }
    function isAndroid(){
        var ua = navigator.userAgent
        var result = ua.match(/(Android)\s+([\d.]+)/),
            isAnd = !!result
        return isAnd
    }
    function isIos(){
        var ua = navigator.userAgent
        var result = ua.match(/.*OS\s([\d_]+)/),
            isIosBrowser = !!result
        return isIosBrowser
    }
    function download(type){
        if (0 && isWeiXin()) {
            if (!$('.cover').length) {
                var html = $('#tips_template').html()
                $('body').append(html)
            }else{
                $('.cover').show()
            }
            var sh = document.documentElement.scrollHeight
            $('.cover').height(sh)
            $('.cover').on('click', function(){
                $('.cover').hide()
            })
        }else{
            if (type == 'ios') {
                window.open("/touting/download")
            }else{
                window.open("/touting/download")
            }
        }
    }
    $(function(){
        bindEvent()
    })
})(window)
