(function(window, undefined){
    var $ = window.$, _animateTimer
    function bindEvent(){
        $('.j_course').on('click', function(){
            $('.select-course').remove()
            $(this).append('<img class="select-course" src="/assets/images/touting/select.png">')
            $('.j_course').removeClass('j_select_course')
            $(this).addClass('j_select_course')
            $("html,body").animate({scrollTop:$(".course-titlte").offset().top}, 300)
        })
        $('.j_input').on('focus', function(){
            $(this).parent().addClass('active')
            $('.error-tips').text('')
        })
        $('.j_input').on('blur', function(){
            $(this).parent().removeClass('active')
        })
        $('.j_input').on('input', function(){
            var mail1 = $.trim($('.j_input1').val()).toLowerCase(), mail2 = $.trim($('.j_input2').val()).toLowerCase(),
                $img = $('.button img'), src = $img.attr('src')
            if (mail1 && mail1==mail2) {
                src = src.replace('button.png', 'button-active2.png')
                $('.button').addClass('active')
            }else{
                src = src.replace('button-active2.png', 'button.png')
                $('.button').removeClass('active')
            }
            $img.attr('src', src)
        })
        //发送ajax请求
        $('.button').on('click', function(){
            var mail1 = $.trim($('.j_input1').val()).toLowerCase(), mail2 = $.trim($('.j_input2').val()).toLowerCase()
            var type = $('.j_select_course').attr('d-type')
            var reg = /^[A-Za-z0-9-_]+([-_.][A-Za-zd]+)*@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,20}){1,2})$/
            if ($(this).hasClass('active') && type) {
                if (!reg.test(mail1)) {
                    $('.error-tips').text('提示：邮箱输入错误')
                    return
                }
                //邮箱和课程类型
                var email = $.trim($('.j_input1').val()), type = $('.j_select_course').attr('d-type')
                //$.ajax({
                //    'type': 'POST',
                //    'url': '/api/touting/getc',
                //    'data': {'sku':type, 'email': email},//ajax请求的参数
                //    'dataType': 'json',
                //    'success': function(data){//ajax返回的结果
                //        if (data.success) {
                //            window.location = data.href;
                //        } else {
                //            alert(data.err_msg);
                //            return false;
                //        }
                //    },
                //    'error': function(){
                //        alert('服务器出小差了~')
                //    }
                //})
                $.ajax({
                    'type': "GET",
                    'url': "/api/order/create_alipay",
                    'data': {
                        'openid': '',
                        'sku': type,
                        'email': email,
                        'test_group': 'o3'
                    },
                    'success': function(data) {
                        if (data.success) {
                            _AP.pay(data.execute_url)
                        } else {
                            alert(data.err_msg);
                        }
                    },
                    'error': function() {
                        alert('服务器出小差了~');
                    }
                })
            }else{
                if (!type) {
                    $('.error-tips').text('提示：请选择套餐')
                    $("html,body").animate({scrollTop:$(".course-titlte").offset().top}, 300)
                } else if ( (!mail1 && !mail2) ) {
                    $('.error-tips').text('提示：请输入邮箱')
                }else if (mail1 != mail2) {
                    $('.error-tips').text('提示：两次输入不一致')
                }
            }
        })
    }
    $(function(){
        bindEvent()
    })
})(window, undefined)
