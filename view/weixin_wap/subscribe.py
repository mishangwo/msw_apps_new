# encoding=utf8
'''
微信内订阅的Wap页面
'''

from bottle import get
from bottle import jinja2_view as view


@get('/weixin/subscribe')
@view('templates/weixin_subscribe.html')
def weixin_subscribe():
    '''
    微信内订阅的Wap页面
    '''
    return {}
