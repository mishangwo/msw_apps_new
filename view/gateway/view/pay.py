# coding: utf-8

from bottle import (
    get,
    post,
    request,
    redirect
)

from ...decorator import add_patch_funcs
from bottle import jinja2_view as view
from service.weixin_service import Weixin_Service
from service.image_service import ImageService
from service.order_service import Order_Service
from service.drupal_service import Drupal_Service
from service.yeepay_service import Yeepay_Service
from service.sku_service import SkuService
from service.common_service import Common_Service
from service.config import (
    GATEWAY_PRODUCT_NAME_GAOSHOU,
    GATEWAY_PRODUCT_NAME_GAOSHOU_TEST,
    GATEWAY_PRODUCT_NAME_BOOK,
    GATEWAY_PRODUCT_NAME_TOUTING,
    GATEWAY_PRODUCT_NAME_QIANGTUI,
    GATEWAY_PRODUCT_NAME_KSXY,
    OSS_ENDPOINT_EXT,
    ORDER_STATUS
)

import time
import json
import base64


@get('/gateway/pay/pc/<product_name>')
def gateway_pay_pc_product_name_get(product_name):
    redirect('/gateway/order/pc/%s' % product_name)


@post('/gateway/pay/pc/<product_name>')
@view('templates/gateway/pay/pc/pay.html')
@add_patch_funcs
def gateway_pay_pc_product_name(product_name):
    if product_name not in [GATEWAY_PRODUCT_NAME_GAOSHOU, GATEWAY_PRODUCT_NAME_TOUTING, GATEWAY_PRODUCT_NAME_BOOK, GATEWAY_PRODUCT_NAME_QIANGTUI, GATEWAY_PRODUCT_NAME_KSXY, GATEWAY_PRODUCT_NAME_GAOSHOU_TEST]:
        return u'数据提交有误'

    email = request.params.get('user_email')
    spbill_create_ip = request.environ.get('REMOTE_ADDR')

    # 计算出SKU
    sku = ''
    if product_name == GATEWAY_PRODUCT_NAME_BOOK:
        sku = 'MS'
        extra_product = request.params.get('extra_product', '')
        if extra_product == '1':
            sku = 'MK'
    elif product_name == GATEWAY_PRODUCT_NAME_KSXY:
        sku = 'KS'
        extra_product = request.params.get('extra_product', '')
        if extra_product == '1':
            sku = 'KQ'
    elif product_name == GATEWAY_PRODUCT_NAME_QIANGTUI:
        sku = 'QT'
        extra_product = request.params.get('extra_product', '')
        if extra_product == '1':
            sku = 'QG'
    elif product_name == GATEWAY_PRODUCT_NAME_TOUTING:
        sku = 'T1'
        extra_product = request.params.get('extra_product', '')
        if extra_product == '1':
            sku = 'T2'
    elif product_name == GATEWAY_PRODUCT_NAME_GAOSHOU:
        sku = request.params.get('sku', '')
    elif product_name == GATEWAY_PRODUCT_NAME_GAOSHOU_TEST:
        sku = 'GAOSHOU_T2'

    if not email or not sku:
        return u'数据提交有误'

    sku_info = SkuService.get_sku_info_by_sku_code(sku)
    if not sku_info:
        return u'找不到SKU信息'

    # 生成微信支付二维码的地址
    if True or product_name == GATEWAY_PRODUCT_NAME_GAOSHOU_TEST:
        success, weixin_code_url, out_trade_no = Weixin_Service().create_native_prepay_order_with_msw_service(email, sku, spbill_create_ip)
    else:
        success, weixin_code_url, out_trade_no = Weixin_Service().create_native_prepay_order(email, sku, spbill_create_ip)
    if not success:
        return u'生成订单失败'

    qrcode_img = ImageService.get_weixin_qrcode_image_url(weixin_code_url)

    ret_data = {
        'email': email,
        'sku': sku,
        'product_name': sku_info.get('body', '').decode('utf-8'),
        'total_fee': sku_info.get('price', 0) / 100.0,
        'weixin_code_url': '%s%s' % (OSS_ENDPOINT_EXT, qrcode_img),
        'wx_out_trade_no': out_trade_no
    }

    return {
        'data': ret_data
    }


@get('/gateway/pay_succeed_pc/<pay_type>')
@view('templates/gateway/pay/pay_succeed_pc.html')
@add_patch_funcs
def gateway_pay_succeed_pc(pay_type):
    """
    支付成功跳转页面（PC版）
    """
    if pay_type == 'alipay':
        otn = request.query.get('out_trade_no', '')
        if not otn:
            redirect('/order/pay_failed')

        is_success = request.query.get('is_success')
        if not is_success or is_success.lower() != 't':
            redirect('/order/pay_failed')

        time.sleep(3)

        order_service = Order_Service()
        order_product = order_service.get_order_product_by_out_trade_no(otn)
        if not order_product:
            redirect('/order/pay_failed')
        else:
            # 需要返回订单编号，产品信息，邮箱以及密码(如果有密码的话)
            order_key = order_product.get('order_key', '')
            email = order_product.get('email', '')
            password = order_product.get('password', '')
            product_name = order_product['products'][0]['name']
            if not password:
                password = u'(你的旧密码)'

            return {
                'order_key': order_key,
                'order_date': time.strftime('%Y/%m/%d %H:%M', time.localtime()),
                'email': email,
                'password': password,
                'product_name': product_name
            }
    elif pay_type == 'yeepay':
        data = request.query.get('data', '')
        encryptkey = request.query.get('encryptkey', '')
        if not data or not encryptkey:
            redirect('/order/pay_failed')

        yeepay_service = Yeepay_Service()
        data = yeepay_service.result_decrypt(data, encryptkey)
        data = json.loads(data)

        time.sleep(2)

        order_service = Order_Service()
        order_product = order_service.get_order_product_by_out_trade_no(data['orderid'])
        if not order_product:
            redirect('/order/pay_failed')
        else:
            # 需要返回订单编号，产品信息，邮箱以及密码(如果有密码的话)
            order_key = order_product.get('order_key', '')
            email = order_product.get('email', '')
            password = order_product.get('password', '')
            product_name = order_product['products'][0]['name']
            if not password:
                password = u'(你的旧密码)'

            return {
                'order_key': order_key,
                'order_date': time.strftime('%Y/%m/%d %H:%M', time.localtime()),
                'email': email,
                'password': password,
                'product_name': product_name
            }
    elif pay_type == 'yeepay_netbank':
        r1_code = request.params.get('r1_Code')
        r2_trxid = request.params.get('r2_TrxId')
        r6_order = request.params.get('r6_Order')
        r9_btype = request.params.get('r9_BType')
        try:
            r9_btype = int(r9_btype)
            r1_code = int(r1_code)
        except Exception:
            redirect('/order/pay_failed')

        if r1_code != 1:
            redirect('/order/pay_failed')

        order_service = Order_Service()
        drupal_service = Drupal_Service()

        # 支付成功，更新数据库，并把订单信息传到Drupal后台
        out_trade_no = r6_order
        order = order_service.get_order_by_out_trade_no(out_trade_no)
        if not order:
            redirect('/order/pay_failed')

        order_product = ''
        if order['status'] != ORDER_STATUS.YEEPAY_SUCCEED:
            order_service.update_order_status_by_otn(out_trade_no, ORDER_STATUS.YEEPAY_SUCCEED)
            # 调用Drupal API，把订单传到Drupal后台并触发邮件
            order_product = drupal_service.post_to_drupal(order, r2_trxid, 'Yeepay', 'PC')
            try:
                order_product = json.loads(order_product)
                order_service.save_order_product(order_product)
            except Exception, e:
                print 'Error saving order products in view. (%s)' % str(e)
                redirect('/order/pay_failed')
        else:
            order_product = order_service.get_order_product_by_out_trade_no(out_trade_no)

        if not order_product:
            redirect('/order/pay_failed')
        else:
            # 需要返回订单编号，产品信息，邮箱以及密码(如果有密码的话)
            order_key = order_product.get('order_key', '')
            email = order_product.get('email', '')
            password = order_product.get('password', '')
            product_name = order_product['products'][0]['name']
            if not password:
                password = u'(你的旧密码)'

            return {
                'order_key': order_key,
                'order_date': time.strftime('%Y/%m/%d %H:%M', time.localtime()),
                'email': email,
                'password': password,
                'product_name': product_name
            }
    elif pay_type == 'weixin':
        otn = request.query.get('out_trade_no', '')
        if not otn:
            redirect('/order/pay_failed')

        order_service = Order_Service()
        order_product = order_service.get_order_product_by_out_trade_no(otn)
        if not order_product:
            redirect('/order/pay_failed')
        else:
            # 需要返回订单编号，产品信息，邮箱以及密码(如果有密码的话)
            order_key = order_product.get('order_key', '')
            email = order_product.get('email', '')
            password = order_product.get('password', '')
            product_name = order_product['products'][0]['name']
            if not password:
                password = u'(你的旧密码)'

            return {
                'order_key': order_key,
                'order_date': time.strftime('%Y/%m/%d %H:%M', time.localtime()),
                'email': email,
                'password': password,
                'product_name': product_name
            }
    else:
        return 'Invalid request'


@get('/gateway/pay_succeed_mobile/<pay_type>')
@view('templates/gateway/pay/pay_succeed_mobile.html')
@add_patch_funcs
def gateway_pay_succeed_mobile(pay_type):
    """
    支付成功跳转页面（Mobile版）
    """
    if pay_type == 'alipay':
        otn = request.query.get('out_trade_no', '')
        if not otn:
            redirect('/order/pay_failed')

        is_success = request.query.get('is_success')
        if not is_success or is_success.lower() != 't':
            redirect('/order/pay_failed')

        time.sleep(3)

        order_service = Order_Service()
        order_product = order_service.get_order_product_by_out_trade_no(otn)
        if not order_product:
            redirect('/order/pay_failed')
        else:
            # 需要返回订单编号，产品信息，邮箱以及密码(如果有密码的话)
            order_key = order_product.get('order_key', '')
            email = order_product.get('email', '')
            password = order_product.get('password', '')
            product_name = order_product['products'][0]['name']
            if not password:
                password = u'(你的旧密码)'

            return {
                'order_key': order_key,
                'order_date': time.strftime('%Y/%m/%d %H:%M', time.localtime()),
                'email': email,
                'password': password,
                'product_name': product_name
            }
    elif pay_type == 'yeepay':
        data = request.query.get('data', '')
        encryptkey = request.query.get('encryptkey', '')
        if not data or not encryptkey:
            redirect('/order/pay_failed')

        yeepay_service = Yeepay_Service()
        data = yeepay_service.result_decrypt(data, encryptkey)
        data = json.loads(data)

        time.sleep(2)

        order_service = Order_Service()
        order_product = order_service.get_order_product_by_out_trade_no(data['orderid'])
        if not order_product:
            redirect('/order/pay_failed')
        else:
            # 需要返回订单编号，产品信息，邮箱以及密码(如果有密码的话)
            order_key = order_product.get('order_key', '')
            email = order_product.get('email', '')
            password = order_product.get('password', '')
            product_name = order_product['products'][0]['name']
            if not password:
                password = u'(你的旧密码)'

            return {
                'order_key': order_key,
                'order_date': time.strftime('%Y/%m/%d %H:%M', time.localtime()),
                'email': email,
                'password': password,
                'product_name': product_name
            }
    elif pay_type == 'yeepay_netbank':
        r1_code = request.params.get('r1_Code')
        r2_trxid = request.params.get('r2_TrxId')
        r6_order = request.params.get('r6_Order')
        r9_btype = request.params.get('r9_BType')
        try:
            r9_btype = int(r9_btype)
            r1_code = int(r1_code)
        except Exception:
            redirect('/order/pay_failed')

        if r1_code != 1:
            redirect('/order/pay_failed')

        order_service = Order_Service()
        drupal_service = Drupal_Service()

        # 支付成功，更新数据库，并把订单信息传到Drupal后台
        out_trade_no = r6_order
        order = order_service.get_order_by_out_trade_no(out_trade_no)
        if not order:
            redirect('/order/pay_failed')

        order_product = ''
        if order['status'] != ORDER_STATUS.YEEPAY_SUCCEED:
            order_service.update_order_status_by_otn(out_trade_no, ORDER_STATUS.YEEPAY_SUCCEED)
            # 调用Drupal API，把订单传到Drupal后台并触发邮件
            order_product = drupal_service.post_to_drupal(order, r2_trxid, 'Yeepay', 'PC')
            try:
                order_product = json.loads(order_product)
                order_service.save_order_product(order_product)
            except Exception, e:
                print 'Error saving order products in view. (%s)' % str(e)
                redirect('/order/pay_failed')
        else:
            order_product = order_service.get_order_product_by_out_trade_no(out_trade_no)

        if not order_product:
            redirect('/order/pay_failed')
        else:
            # 需要返回订单编号，产品信息，邮箱以及密码(如果有密码的话)
            order_key = order_product.get('order_key', '')
            email = order_product.get('email', '')
            password = order_product.get('password', '')
            product_name = order_product['products'][0]['name']
            if not password:
                password = u'(你的旧密码)'

            return {
                'order_key': order_key,
                'order_date': time.strftime('%Y/%m/%d %H:%M', time.localtime()),
                'email': email,
                'password': password,
                'product_name': product_name
            }
    elif pay_type == 'weixin':
        otn = request.query.get('out_trade_no', '')
        if not otn:
            redirect('/order/pay_failed')

        order_service = Order_Service()
        order_product = order_service.get_order_product_by_out_trade_no(otn)
        if not order_product:
            redirect('/order/pay_failed')
        else:
            # 需要返回订单编号，产品信息，邮箱以及密码(如果有密码的话)
            order_key = order_product.get('order_key', '')
            email = order_product.get('email', '')
            password = order_product.get('password', '')
            product_name = order_product['products'][0]['name']
            if not password:
                password = u'(你的旧密码)'

            return {
                'order_key': order_key,
                'order_date': time.strftime('%Y/%m/%d %H:%M', time.localtime()),
                'email': email,
                'password': password,
                'product_name': product_name
            }
    else:
        return 'Invalid request'


@get('/gateway/pay/mobile/wechat/<product_name>')
@view('templates/gateway/pay/mobile/pay_wechat.html')
@add_patch_funcs
def gateway_pay_mobile_wechat_product_name(product_name):
    """
    微信内Mobile支付
    """
    wx_service = Weixin_Service()

    code = request.query.get('code')
    state = request.query.get('state')

    if not code or not state:
        return '微信授权跳转失败'

    try:
        sku_and_email = base64.decodestring(state)
        sku, email = sku_and_email.split(',', 1)
    except Exception:
        return u'数据解析失败'

    # 获取用户的OpenID
    if True or sku == 'GAOSHOU_T2':
        access_token, openid = wx_service.get_oauth_access_token_and_openid_by_msw_service(code)
    else:
        access_token, openid = wx_service.get_oauth_access_token_and_openid(code)

    if not access_token or not openid:
        redirect('/gateway/order/%s' % product_name)

    sku_info = SkuService.get_sku_info_by_sku_code(sku)
    if not sku_info:
        return u'找不到SKU信息'

    return {
        'openid': openid,
        'sku': sku,
        'email': email,
        'product_name': product_name
    }


@get('/gateway/pay/mobile/<product_name>')
@view('templates/gateway/pay/mobile/pay.html')
@add_patch_funcs
def gateway_pay_mobile_product_name(product_name):
    """
    非微信内mobile支付
    """
    state = request.query.get('state')
    try:
        sku_and_email = base64.decodestring(state)
        sku, email = sku_and_email.split(',', 1)
    except Exception:
        return u'数据解析失败'

    if not email or not sku:
        return u'数据提交有误'

    sku_info = SkuService.get_sku_info_by_sku_code(sku)
    if not sku_info:
        return u'找不到SKU信息'

    ret_data = {
        'email': email,
        'sku': sku,
    }

    return ret_data


@get('/gateway/pay/mobile/wechat/pay.htm')
@view('templates/order/pay.htm')
def gateway_pay_mobile_wechat_pay_htm():
    '''
    支付宝跳转页面
    '''
    return {
        'is_android': Common_Service.is_android(),
        'is_ios': Common_Service.is_ios()
    }


@get('/gateway/pay/mobile/wechat/pay_v2.htm')
@view('templates/order/pay_v2.htm')
def gateway_pay_mobile_wechat_pay_v2_htm():
    '''
    新版支付宝跳转页面
    '''
    return {}
