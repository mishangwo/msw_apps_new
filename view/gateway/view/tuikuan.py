# coding: utf-8

from bottle import (
    abort,
    get,
    post,
    redirect,
    request
)
import requests
import re
import json

from ...decorator import add_patch_funcs
from bottle import jinja2_view as view
from validate_email import validate_email
from service.common_service import Common_Service

@get('/gateway/tuikuan/')
def msw_tuikuan_entry():
    if Common_Service.is_mobile():
        redirect('/gateway/m/tuikuan/')
    else:
        redirect('/gateway/pc/tuikuan/')


@get('/gateway/pc/tuikuan/')
@view('templates/tuikuan/tuikuan_pc.html')
@add_patch_funcs
def tuikuan_index():
    return {}

@get('/gateway/m/tuikuan/')
@view('templates/tuikuan/tuikuan_mobile.html')
@add_patch_funcs
def tuikuan_index_mobile():
    return {}


@post('/gateway/tuikuan/submit')
def tuikuan_submit():
    email = request.forms.get('user_email').strip()
    stdno = request.forms.get('user_stdno').strip()
    cause = request.forms.getlist('cause')
    source =  request.forms.get('source').strip()

    isFailed = 0

    if not email or not stdno:
        isFailed = 1

    match = re.match(r'^[0-9a-zA-Z]{1,32}$', stdno)
    if not match:
        isFailed = 1

    if not validate_email(email):
        isFailed = 1


    if isFailed == 1:
         if source == 'pc':
            redirect('/gateway/tuikuan/success')
         else:
            redirect('/gateway/m/tuikuan/success')



    data = {
        'email': email,
        'stdno': stdno,
        'cause': cause
    }
    s = requests.Session()
    s.headers.update({'Content-Type': 'application/json'})
    resp = s.post('https://mobile-backend.mishangwo.com/order/refund', data=json.dumps(data), verify=True)
    status_code = resp.status_code
    resp_content = resp.content

    if resp_content == 'SUCCESS':
         if source == 'pc':
            redirect('/gateway/tuikuan/success')
         else:
            redirect('/gateway/m/tuikuan/success')
    else:
         if source == 'pc':
            redirect('/gateway/tuikuan/failed')
         else:
            redirect('/gateway/m/tuikuan/failed')


@get('/gateway/tuikuan/success')
@view('templates/tuikuan/tuikuan_success_pc.html')
@add_patch_funcs
def tuikuan_success():
    return {}

@get('/gateway/m/tuikuan/success')
@view('templates/tuikuan/tuikuan_success_mobile.html')
@add_patch_funcs
def tuikuan_mobile_success():
    return {}


@get('/gateway/tuikuan/failed')
@view('templates/tuikuan/tuikuan_failed_pc.html')
@add_patch_funcs
def tuikuan_failed():
    return {}


@get('/gateway/m/tuikuan/failed')
@view('templates/tuikuan/tuikuan_failed_mobile.html')
@add_patch_funcs
def tuikuan_mobile_failed():
    return {}