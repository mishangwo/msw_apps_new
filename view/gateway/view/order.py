# coding: utf-8

from bottle import (
    abort,
    get,
    redirect
)

from ...decorator import add_patch_funcs
from bottle import jinja2_view as view

from service.common_service import Common_Service
from service.config import (
    GATEWAY_PRODUCT_NAME_GAOSHOU,
    GATEWAY_PRODUCT_NAME_GAOSHOU_TEST,
    GATEWAY_PRODUCT_NAME_BOOK,
    GATEWAY_PRODUCT_NAME_TOUTING,
    GATEWAY_PRODUCT_NAME_QIANGTUI,
    GATEWAY_PRODUCT_NAME_KSXY
)


# Main entry
@get('/gateway/order/<product_name>')
def msw_order_entry(product_name):
    if product_name not in [GATEWAY_PRODUCT_NAME_GAOSHOU, GATEWAY_PRODUCT_NAME_TOUTING, GATEWAY_PRODUCT_NAME_BOOK, GATEWAY_PRODUCT_NAME_QIANGTUI, GATEWAY_PRODUCT_NAME_KSXY, GATEWAY_PRODUCT_NAME_GAOSHOU_TEST]:
        abort(404, '')

    if Common_Service.is_mobile():
        redirect('/gateway/order/mobile/%s' % product_name)
    else:
        redirect('/gateway/order/pc/%s' % product_name)


# PC
@get('/gateway/order/pc/book')
@view('templates/gateway/order/pc/book.html')
@add_patch_funcs
def book_pc():
    return {}


@get('/gateway/order/pc/qiangtui')
@view('templates/gateway/order/pc/qiangtui.html')
@add_patch_funcs
def qiangtui_pc():
    return {}


@get('/gateway/order/pc/gaoshou')
@view('templates/gateway/order/pc/gaoshou.html')
@add_patch_funcs
def gaoshou_pc():
    return {}


# @get('/gateway/order/pc/gaoshou_test')
# @view('templates/gateway/order/pc/gaoshou_test.html')
# @add_patch_funcs
# def gaoshou_test_pc():
#     return {}


@get('/gateway/order/pc/touting')
@view('templates/gateway/order/pc/touting.html')
@add_patch_funcs
def touting_pc():
    return {}


@get('/gateway/order/pc/ksxy')
@view('templates/gateway/order/pc/ksxy.html')
@add_patch_funcs
def ksxy_pc():
    return {}


# Mobile
@get('/gateway/order/mobile/book')
@view('templates/gateway/order/mobile/book.html')
@add_patch_funcs
def book_mobile():
    return {}


@get('/gateway/order/mobile/gaoshou')
@view('templates/gateway/order/mobile/gaoshou.html')
@add_patch_funcs
def gaoshou_mobile():
    return {}


# @get('/gateway/order/mobile/gaoshou_test')
# @view('templates/gateway/order/mobile/gaoshou_test.html')
# @add_patch_funcs
# def gaoshou_test_mobile():
#     return {}


@get('/gateway/order/mobile/touting')
@view('templates/gateway/order/mobile/touting.html')
@add_patch_funcs
def touting_mobile():
    return {}


@get('/gateway/order/mobile/qiangtui')
@view('templates/gateway/order/mobile/qiangtui.html')
@add_patch_funcs
def qiangtui_mobile():
    return {}


@get('/gateway/order/mobile/ksxy')
@view('templates/gateway/order/mobile/ksxy.html')
@add_patch_funcs
def ksxy_mobile():
    return {}
