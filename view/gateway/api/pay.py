# encoding=utf8
'''
迷上我支付网入口，以后迷上我所有支付相关的业务都会走这个入口
'''

from bottle import (
    get,
    post,
    request,
    redirect
)
import base64
import urllib

from service.order_service import Order_Service
from service.alipay_service import Alipay_Service_V2, Alipay_Service
from service.yeepay_pc import YeepayPcService
from service.yeepay_service import Yeepay_Service
from service.sku_service import SkuService
from service.weixin_service import Weixin_Service
from service.common_service import Common_Service
from service.config import (
    ALIPAY_V2_SERVICE_PC,
    GATEWAY_PAY_TYPE_ALIPAY,
    GATEWAY_PAY_TYPE_YEEPAY,
    GATEWAY_VALIDATE_PAY_TYPE,
    GATEWAY_PRODUCT_NAME_GAOSHOU,
    GATEWAY_PRODUCT_NAME_GAOSHOU_TEST,
    GATEWAY_PRODUCT_NAME_BOOK,
    GATEWAY_PRODUCT_NAME_TOUTING,
    GATEWAY_PRODUCT_NAME_QIANGTUI,
    GATEWAY_PRODUCT_NAME_KSXY,
    WX_APP_ID,
    MSW_SERVICE_WX_APP_ID
)


@post('/gateway/api/pay/submit/pc')
def gateway_pay_submit_pc():
    """
    PC端支付网关
    接收各个站点提交的参数，然后redirect到支付平台
    """
    sku = request.params.get('sku')
    email = request.params.get('email')
    spbill_create_ip = request.environ.get('REMOTE_ADDR')
    pay_type = request.params.get('pay_type')
    extra_info = request.params.get('extra_info', '')
    ua = request.headers.get('User-Agent', '')

    if not email or not sku or pay_type not in GATEWAY_VALIDATE_PAY_TYPE:
        return {
            'success': False,
            'err_msg': '创建订单失败！(参数不完整)',
        }

    order_service = Order_Service()
    # 在系统中创建订单
    succeed, order_info = order_service.create_order(email, '', sku, spbill_create_ip, pay_type, '0')
    if not succeed:
        return {
            'success': False,
            'err_msg': u'创建支付宝订单失败！'
        }

    # 获取Token成功，组建提交到支付宝的支付请求URL和数据，返回给客户端
    if pay_type == GATEWAY_PAY_TYPE_ALIPAY:
        alipay_service = Alipay_Service_V2()
        execute_url = alipay_service.get_alipay_execute_url(order_info, ALIPAY_V2_SERVICE_PC)
    elif pay_type == GATEWAY_PAY_TYPE_YEEPAY:
        otn = order_info['out_trade_no']
        amount = order_info['total_fee'] / 100.0
        if extra_info == 'YJZF-NET':  # 快捷支付
            otn = order_info['out_trade_no']
            amount = order_info['total_fee']
            product_name = order_info['body']
            execute_url = Yeepay_Service().get_yeepay_execute_url(otn, amount, product_name, ua, spbill_create_ip, email, 'pc')
        else:
            execute_url = YeepayPcService.get_request_url(otn, amount, extra_info, email)
    return {
        'success': True,
        'link': urllib.quote_plus(execute_url)
    }


@post('/gateway/api/pay/submit/mobile')
def gateway_pay_submit_mobile():
    """
    Mobile端支付网关
    接收各个站点提交的参数，然后redirect到支付平台
    """
    sku = request.params.get('sku')
    email = request.params.get('email')
    spbill_create_ip = request.environ.get('REMOTE_ADDR')
    pay_type = request.params.get('pay_type')
    openid = request.params.get('openid', '')
    ua = request.headers.get('User-Agent', '')

    if not email or not sku or pay_type not in GATEWAY_VALIDATE_PAY_TYPE:
        return {
            'success': False,
            'err_msg': '创建订单失败！(参数不完整)',
        }

    order_service = Order_Service()
    # 在系统中创建订单
    succeed, order_info = order_service.create_order(email, openid, sku, spbill_create_ip, pay_type, '0')
    if not succeed:
        return {
            'success': False,
            'err_msg': u'创建支付宝订单失败！'
        }

    # 获取Token成功，组建提交到支付宝的支付请求URL和数据，返回给客户端
    if pay_type == GATEWAY_PAY_TYPE_ALIPAY:
        # iframe被封，暂时先用回老的方案
        # alipay_service = Alipay_Service_V2()
        # if Common_Service.is_wechat():
        #     execute_url = urllib.quote_plus(alipay_service.get_alipay_execute_url(order_info))
        # else:
        #     execute_url = alipay_service.get_alipay_execute_url(order_info)
        alipay_service = Alipay_Service()
        # 根据订单信息，请求支付宝的授权接口，返回request token
        succeed, info = alipay_service.get_request_token(order_info)
        if not succeed:
            return {
                'success': False,
                'err_msg': info
            }

        # 获取Token成功，组建提交到支付宝的支付请求URL和数据，返回给客户端
        execute_url = alipay_service.get_alipay_execute_url(info)
    elif pay_type == GATEWAY_PAY_TYPE_YEEPAY:
        otn = order_info['out_trade_no']
        amount = order_info['total_fee']
        product_name = order_info['body']
        execute_url = Yeepay_Service().get_yeepay_execute_url(otn, amount, product_name, ua, spbill_create_ip, email)
    return {
        'success': True,
        'execute_url': execute_url
    }


@post('/gateway/api/pay/submit/mobile/wechat')
def gateway_pay_submit_mobile_wechat():
    """
    Mobile端微信内提交订单到后台
    除了微信支付外，其他的支付请求都提交到/gateway/api/pay/submit/mobile
    之所以需要这个单独的接口是因为其他的支付接口，提交数据后都是返回一个跳转
    地址，但微信支付因为是启动native支付页面，提交后是返回的是jssdk相关的一些
    数据，所以需要这个单独的接口
    """
    sku = request.params.get('sku')
    email = request.params.get('email')
    spbill_create_ip = request.environ.get('REMOTE_ADDR')
    openid = request.params.get('openid', '')

    if not openid or not email or not sku:
        return {
            'success': False,
            'err_msg': '创建订单失败！(参数不完整)',
        }

    wx_service = Weixin_Service()

    # 在系统中创建订单
    if True or sku == 'GAOSHOU_T2':
        succeed, payinfo = wx_service.create_prepay_order_with_msw_service(email, openid, sku, spbill_create_ip, '0')
    else:
        succeed, payinfo = wx_service.create_prepay_order(email, openid, sku, spbill_create_ip, '0')

    if succeed:
        if True or sku == 'GAOSHOU_T2':
            sdkinfo = wx_service.get_jssdk_info_with_msw_service(request.url.replace('http://', 'http://'))
        else:
            sdkinfo = wx_service.get_jssdk_info(request.url.replace('http://', 'http://'))
        return {
            'success': True,
            'sdkinfo': sdkinfo,
            'payinfo': payinfo
        }
    else:
        return {
            'success': False,
            'err_msg': '创建订单失败！(请联系管理员)',
            'sdkinfo': '',
            'payinfo': ''
        }


@get('/gateway/api/pay/status')
def gateway_api_pay_status():
    """
    查询支付状态
    """
    _type = request.query.get('type', '')
    otn = request.query.get('out_trade_no', '')
    if _type != 'weixin' or not otn:
        return {
            'success': False,
            'paid': False
        }

    # 检查该OTN有没有对应的产品信息，如果有，则说明支付成功了
    order_service = Order_Service()
    order_product = order_service.get_order_product_by_out_trade_no(otn)
    if order_product:
        return {
            'success': True,
            'paid': True
        }

    return {
        'success': True,
        'paid': False
    }


@get('/gateway/jump')
def gateway_jump():
    """
    跳转
    """
    url = request.query.get('url')
    if not url:
        return 'No URL Found!!!'
    redirect(url)


@post('/gateway/api/pay/mobile/before_redirect')
def gateway_api_pay_mobile_submit():
    """
    手机支付统一数据提交入口
    这个接口需要获取SKU和邮箱信息，然后生成订单
    如果用户在微信打开，那还需要先微信授权，获取openid，因为要支持微信支付
    否则不需要跳微信授权，只需要支持alipay和yeepay
    """
    product_name = request.forms.get('product_name', '')
    if product_name not in [GATEWAY_PRODUCT_NAME_QIANGTUI, GATEWAY_PRODUCT_NAME_KSXY, GATEWAY_PRODUCT_NAME_GAOSHOU, GATEWAY_PRODUCT_NAME_TOUTING, GATEWAY_PRODUCT_NAME_BOOK, GATEWAY_PRODUCT_NAME_GAOSHOU_TEST]:
        return u'数据提交有误'

    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')

    # 计算出SKU
    sku = ''
    if product_name == GATEWAY_PRODUCT_NAME_BOOK:
        sku = 'MS'
        extra_product = request.params.get('extra_product', '')
        if extra_product == '1':
            sku = 'MK'
    elif product_name == GATEWAY_PRODUCT_NAME_QIANGTUI:
        sku = 'QT'
        extra_product = request.params.get('extra_product', '')
        if extra_product == '1':
            sku = 'QG'
    elif product_name == GATEWAY_PRODUCT_NAME_KSXY:
        sku = 'KS'
        extra_product = request.params.get('extra_product', '')
        if extra_product == '1':
            sku = 'KQ'
    elif product_name == GATEWAY_PRODUCT_NAME_TOUTING:
        sku = request.params.get('sku', '')
    elif product_name == GATEWAY_PRODUCT_NAME_GAOSHOU:
        sku = request.params.get('sku', '')
    elif product_name == GATEWAY_PRODUCT_NAME_GAOSHOU_TEST:
        sku = 'GAOSHOU_T2'

    if not email or not sku:
        return u'Invalid request'

    sku_info = SkuService.get_sku_info_by_sku_code(sku)
    if not sku_info:
        return u'找不到SKU信息'

    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = '/gateway/pay/mobile/%s?state=%s' % (product_name, state)
    if Common_Service.is_wechat():
        if True or product_name == GATEWAY_PRODUCT_NAME_GAOSHOU_TEST:
            href = 'https://open.weixin.qq.com/connect/oauth2/authorize?state=%s&appid=%s&redirect_uri=https://apps.mishangwo.com/gateway/pay/mobile/wechat/%s?response_type=code&scope=snsapi_base#wechat_redirect' % (state, MSW_SERVICE_WX_APP_ID, product_name)
        else:
            href = 'https://open.weixin.qq.com/connect/oauth2/authorize?state=%s&appid=%s&redirect_uri=https://apps.mishangwo.com/gateway/pay/mobile/wechat/%s?response_type=code&scope=snsapi_base#wechat_redirect' % (state, WX_APP_ID, product_name)

    if request.is_ajax:
        return {
            'success': True,
            'execute_url': href
        }
    else:
        redirect(href)
