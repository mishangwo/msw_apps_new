# encoding=utf8
'''
邮件提交接口
'''

from bottle import (
    post,
    request,
)
import hashlib
import requests
from validate_email import validate_email

import re

from pymongo import MongoClient
import pymongo
import datetime
import os

MONGO_DB_HOST = 'msw_mongo01'
MONGO_DB_PORT = 27017
MONGO_DB_NAME = 'msw'



@post('/gateway/api/edm/subscribe')
def gateway_api_edm_subscribe():

    real_ip = request.headers['X-Forwarded-For']
    ipFobiddenTxt = os.getcwd()+"/forbidip.txt";

    if real_ip and os.path.exists(ipFobiddenTxt):
        with open(ipFobiddenTxt, 'r') as file_to_read:
            while True:
                lines = file_to_read.readline()
                if not lines:
                    break
                if lines.strip("\n") == real_ip:
                     return {
                         'succeed': False
                     }

    URL = [
        'http://mailsc.mishangwo.org/form.php?form=1',
        'http://mailsc.mishangwo.com.cn/form.php?form=1',
        'http://submail.mishangwo.net/form.php?form=1',
        'http://lists.mishangwo.com/app/form.php?form=1',
    ]
    email = request.params.get('email', '').strip()
    name = request.params.get('name', '').strip()


    match = re.match(r'^[＼x80-＼xffa-zA-Z][＼x80-＼xffa-zA-Z0-9_\.]{0,60}$', name)
    if match:
        nameVal = True
    else:
        return {
            'succeed': False
        }


    if not email or not name:
        return {
            'succeed': False
        }
    if not validate_email(email):
        return {
            'succeed': False
        }

    if real_ip:
        mongo = MongoClient(host=MONGO_DB_HOST, port=MONGO_DB_PORT)[MONGO_DB_NAME]
        collection = mongo.userapi
        collection.ensure_index([("time", pymongo.ASCENDING)], expireAfterSeconds=7200)
        ipquery = list(collection.find({'ip': real_ip}))
        if len(ipquery) > 0:
            if ipquery[0]['count'] > 5:
                with open(ipFobiddenTxt,'a') as f:
                    f.write(real_ip+"\n")
                return {
                    'succeed': False
                }
            else:
                collection.save({"_id": ipquery[0]['_id'], "ip":real_ip,"time": datetime.datetime.utcnow(),"count":ipquery[0]['count']+1})
        else:
            collection.insert({"ip":real_ip,"time": datetime.datetime.utcnow(),"count":1})


    payload = {
        'email': email,
        'CustomFields[12]': name,
        'format': 'h'
    }
    index = int(int(hashlib.md5(email).hexdigest(), 16)) % len(URL)
    _url = URL[index]
    print 'Dispatching %s to %s.' % (email, _url)
    try:
        requests.post(_url, data=payload)
    except Exception:
        return {
            'succeed': False
        }

    return {
        'succeed': True
    }
