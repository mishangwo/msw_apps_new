# coding: utf-8

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from bottle import (
    abort,
    get,
    redirect,
    post,
    request
)

from ..decorator import add_patch_funcs
from bottle import jinja2_view as view

import time

from service.activities_service import Activities_Service
from service.alipay_activities_service import Alipay_Activities_Service
from service.yeepay_activities_service import Yeepay_Activities_Service
from service.sku_service import SkuService
from service.weixin_activities_service import Weixin_Activities_Service
from service.common_service import Common_Service
from service.config import (
    GATEWAY_PAY_TYPE_ALIPAY,
    GATEWAY_PAY_TYPE_YEEPAY,
    GATEWAY_VALIDATE_PAY_TYPE,
    WX_APP_ID,
    MSW_SERVICE_WX_APP_ID,
    ACT_INFO
)

import base64
import urllib

ACTIVITIES_SKU = 'SDWH'
ACTIVITIES_PRODUCT_NAME = 'sidiwanhui'


@get('/activities/mobile/sidiwanhui')
@view('templates/activities/mobile/over.html')
@add_patch_funcs
def wanhui_mobile():
    sku = ACTIVITIES_SKU
    return {
        "title": ACT_INFO[sku]['body'],
        "sku": sku,
        "price": ACT_INFO[sku]['price'],
        "product_name": ACTIVITIES_PRODUCT_NAME
    }


@post('/activities/mobile/pay/before_redirect')
def activities_pay_mobile_submit():
    """
    手机支付统一数据提交入口
    这个接口需要获取SKU和邮箱信息，然后生成订单
    如果用户在微信打开，那还需要先微信授权，获取openid，因为要支持微信支付
    否则不需要跳微信授权，只需要支持alipay和yeepay
    """
    product_name = request.forms.get('product_name', '')

    if product_name not in [ACTIVITIES_PRODUCT_NAME]:
        return u'数据提交有误'

    email = request.forms.get('email', '')
    username = request.forms.get('username', '')
    telephone = request.forms.get('telephone', '')

    if not email or not username or not telephone:
        return u'数据提交有误'

    sku = ACTIVITIES_SKU

    #state = base64.encodestring('%s,%s,%s,%s' % (sku, email, username, telephone)).strip()
    state = '%s,%s,%s,%s' % (sku, email, username, telephone)
    href = '/activities/mobile/pay/%s?state=%s' % (product_name, state)
    if Common_Service.is_wechat():
       #href = 'https://open.weixin.qq.com/connect/oauth2/authorize?state=%s&appid=%s&redirect_uri=https://apps.mishangwo.com/activities/mobile/pay/wechat/%s?response_type=code&scope=snsapi_base#wechat_redirect' % (state, MSW_SERVICE_WX_APP_ID, product_name)
       href = 'https://open.weixin.qq.com/connect/oauth2/authorize?state=%s&appid=%s&redirect_uri=https://apps.mishangwo.com/activities/mobile/pay/wechat/%s?response_type=code&scope=snsapi_base#wechat_redirect' % (state, MSW_SERVICE_WX_APP_ID, product_name)
    if request.is_ajax:
        return {
            'success': True,
            'execute_url': href
        }
    else:
        redirect(href)




@get('/activities/mobile/pay/<product_name>')
@view('templates/activities/mobile/pay.html')
@add_patch_funcs
def activities_pay_mobile_product_name(product_name):
    """
    非微信内mobile支付
    """
    state = request.query.get('state')
    try:
        #sku_and_email_and_username_and_telephone = base64.decodestring(state)
        sku, email, username, telephone = state.split(',', 3)
    except Exception:
        return u'数据解析失败'

    if not email or not sku or not username or not telephone:
        return u'数据提交有误'

    ret_data = {
        'email': email,
        'sku': ACTIVITIES_SKU,
        'username': username,
        'telephone': telephone
    }

    return ret_data




@post('/activities/mobile/pay/submit')
def activities_pay_submit_mobile():
    """
    Mobile端支付网关
    接收各个站点提交的参数，然后redirect到支付平台
    """
    sku = request.params.get('sku')
    email = request.params.get('email')
    username =  request.params.get('username')
    telephone =  request.params.get('telephone')

    spbill_create_ip = request.environ.get('REMOTE_ADDR')
    pay_type = request.params.get('pay_type')
    openid = request.params.get('openid', '')
    ua = request.headers.get('User-Agent', '')


    if not email or not username or not telephone or not sku or pay_type not in GATEWAY_VALIDATE_PAY_TYPE:
        return {
            'success': False,
            'err_msg': '创建订单失败！(参数不完整)',
        }

    activities_service = Activities_Service()
    # 在系统中创建订单
    succeed, order_info = activities_service.create_order(email, username, telephone, openid, sku, spbill_create_ip, pay_type)
    if not succeed:
        return {
            'success': False,
            'err_msg': u'创建支付宝订单失败！'
        }

    # 获取Token成功，组建提交到支付宝的支付请求URL和数据，返回给客户端
    if pay_type == GATEWAY_PAY_TYPE_ALIPAY:
        # iframe被封，暂时先用回老的方案
        # alipay_service = Alipay_Service_V2()
        # if Common_Service.is_wechat():
        #     execute_url = urllib.quote_plus(alipay_service.get_alipay_execute_url(order_info))
        # else:
        #     execute_url = alipay_service.get_alipay_execute_url(order_info)
        alipay_service = Alipay_Activities_Service()
        # 根据订单信息，请求支付宝的授权接口，返回request token
        succeed, info = alipay_service.get_request_token(order_info)
        if not succeed:
            return {
                'success': False,
                'err_msg': info
            }

        # 获取Token成功，组建提交到支付宝的支付请求URL和数据，返回给客户端
        execute_url = alipay_service.get_alipay_execute_url(info)
    elif pay_type == GATEWAY_PAY_TYPE_YEEPAY:
        otn = order_info['out_trade_no']
        amount = order_info['total_fee']
        product_name = order_info['body']
        execute_url = Yeepay_Activities_Service().get_yeepay_execute_url(otn, amount, product_name, ua, spbill_create_ip, email)
    return {
        'success': True,
        'execute_url': execute_url
    }



@get('/activities/mobile/pay/wechat/<product_name>')
@view('templates/activities/mobile/pay_wechat.html')
@add_patch_funcs
def activities_pay_mobile_wechat_product_name(product_name):
    """
    微信内Mobile支付
    """
    wx_service = Weixin_Activities_Service()

    code = request.query.get('code')
    state = request.query.get('state')

    if not code or not state:
        return '微信授权跳转失败'

    try:
        #sku_and_email_and_username_and_telephone = base64.decodestring(state)
        sku, email, username, telephone = state.split(',', 3)
    except Exception:
        return u'数据解析失败'

    # 获取用户的OpenID
    access_token, openid = wx_service.get_oauth_access_token_and_openid(code)

    if not access_token or not openid:
        abort(404, '')

    sku_info = ACT_INFO[sku]
    if not sku_info:
        return u'找不到SKU信息'

    return {
        'openid': openid,
        'sku': sku,
        'email': email,
        'product_name': product_name,
        'username': username,
        'telephone': telephone
    }



@post('/activities/mobile/pay/submit/wechat')
def activities_pay_submit_mobile_wechat():
    """
    Mobile端微信内提交订单到后台
    除了微信支付外，其他的支付请求都提交到/gateway/api/pay/submit/mobile
    之所以需要这个单独的接口是因为其他的支付接口，提交数据后都是返回一个跳转
    地址，但微信支付因为是启动native支付页面，提交后是返回的是jssdk相关的一些
    数据，所以需要这个单独的接口
    """
    sku = request.params.get('sku')
    email = request.params.get('email')
    spbill_create_ip = request.environ.get('REMOTE_ADDR')
    openid = request.params.get('openid', '')
    username = request.params.get('username', '')
    telephone = request.params.get('telephone', '')

    if not openid or not email or not sku or not username or not telephone:
        return {
            'success': False,
            'err_msg': '创建订单失败！(参数不完整)',
        }

    wx_service = Weixin_Activities_Service()

    # 在系统中创建订单
    succeed, payinfo = wx_service.create_prepay_order(email, username, telephone, openid, sku, spbill_create_ip)

    if succeed:
        sdkinfo = wx_service.get_jssdk_info(request.url.replace('http://', 'http://'))
        return {
            'success': True,
            'sdkinfo': sdkinfo,
            'payinfo': payinfo
        }
    else:
        return {
            'success': False,
            'err_msg': '创建订单失败！(请联系管理员)',
            'sdkinfo': '',
            'payinfo': ''
        }



@get('/activities/pay_succeed')
@view('templates/activities/success.html')
def activities_pay_succeed():
    '''
    支付成功
    目前这个页面用于微信支持成功后的跳转，alipay和yeepay的支付跳转页面分别为/apilay/return_url和yeepay/fcallback
    '''
    otn = request.query.get('otn', '')
    if not otn:
        redirect('/activities/pay_failed')

    # 根据订单号调用查找跟这个订单相关的产品和用户信息
    # 产品和用户信息是依赖微信支付回调才会产生的，因此不能100%保证能
    # 拿到数据。为了增加成功的可能性，我们在这个页面sleep 2s再请求数据并返回
    time.sleep(2)

    order_service = Activities_Service()
    order_product = order_service.get_order_by_out_trade_no(otn)
    if not order_product:
        redirect('/activities/pay_failed?otn=%s' % otn)
    else:
        email = order_product.get('email', '')
        product_name = order_product.get('body', '')
        username = order_product.get('username', '')
        telephone = order_product.get('telephone', '')

        return {
            'order_date': time.strftime('%Y/%m/%d %H:%M', time.localtime()),
            'email': email,
            'username': username,
            'telephone': telephone,
            'product_name': product_name
        }


@get('/activities/pay_failed')
@view('templates/activities/failed.html')
def activities_pay_failed():
    '''
    支付失败
    '''
    otn = request.query.get('otn', '')
    return {
        'otn': otn
    }


@get('/activities/mobile/pay/wechat/pay.htm')
@view('templates/activities/pay.htm')
def activities_pay_mobile_wechat_pay_htm():
    '''
    支付宝跳转页面
    '''
    return {
        'is_android': Common_Service.is_android(),
        'is_ios': Common_Service.is_ios()
    }