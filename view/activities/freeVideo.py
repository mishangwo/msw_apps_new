# coding: utf-8

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from bottle import (
    abort,
    get,
    post,
    redirect,
    request,
    response
)
import requests
import re
import json

from ..decorator import add_patch_funcs
from bottle import jinja2_view as view
from validate_email import validate_email
from service.common_service import Common_Service


@get('/activities/m/freeVideo')
@view('templates/activities/mobile/freeVideo.html')
@add_patch_funcs
def get_freeVideo_m():
    pcookie = request.get_cookie("msw_act_phone")
    if  not pcookie:
        return {
            "title": "视频：内心建设的3个途径",
        }

    data = {
        'phone': pcookie
    }
    s = requests.Session()
    s.headers.update({'Content-Type': 'application/json'})
    resp = s.post('https://mobile-backend.mishangwo.com/serve/verifySts', data=json.dumps(data), verify=True)
    status_code = resp.status_code
    resp_content = resp.content
    if resp_content != 'MSG_SUCCESS':
        return {
            "title": "视频：内心建设的3个方法",
        }
    else:
        redirect('/activities/m/freeVideo/show/?q=1')





@post('/activities/m/freeVideo/code/')
def freevideo_code_m():
    phoneNum = request.POST.get('phone')

    match = re.match(r'^1[3456789]\d{9}$', phoneNum)
    if not match:
        return 'MSG_1'

    data = {
        'phone': phoneNum,
    }
    s = requests.Session()
    s.headers.update({'Content-Type': 'application/json'})
    resp = s.post('https://mobile-backend.mishangwo.com/serve/sendCode', data=json.dumps(data), verify=True)
    status_code = resp.status_code
    resp_content = resp.content
    return resp_content

@post('/activities/m/freeVideo/submit/')
def freevideo_submit_m():
    phoneNum = request.POST.get('phone')
    code = request.POST.get('code')
    match = re.match(r'^1[3456789]\d{9}$', phoneNum)
    if not match:
        return 'MSG_1'
    matchCode = re.match(r'^[0-9]{6}$', code)
    if not matchCode:
        return 'MSG_4'

    data = {
        'phone': phoneNum,
        'code': code
    }
    s = requests.Session()
    s.headers.update({'Content-Type': 'application/json'})
    resp = s.post('https://mobile-backend.mishangwo.com/serve/verifyCode', data=json.dumps(data), verify=True)
    status_code = resp.status_code
    resp_content = resp.content
    if resp_content == 'MSG_SUCCESS':
         response.set_cookie("msw_act_phone", phoneNum, max_age=86400*356, path="/" )

    return resp_content


@get('/activities/m/freeVideo/show/')
@view('templates/activities/mobile/freeVideoShow.html')
@add_patch_funcs
def show_freeVideo_m():
    q= request.query.get('q')
    pcookie = request.get_cookie("msw_act_phone")
    if not pcookie:
        redirect('/activities/m/freeVideo/')

    if  not q==1:
        data = {
            'phone': pcookie
        }
        s = requests.Session()
        s.headers.update({'Content-Type': 'application/json'})
        resp = s.post('https://mobile-backend.mishangwo.com/serve/verifySts', data=json.dumps(data), verify=True)
        status_code = resp.status_code
        resp_content = resp.content
        if resp_content != 'MSG_SUCCESS':
            redirect('/activities/m/freeVideo/')


    return {
        "title": "视频：内心建设的3个途径"
    }





