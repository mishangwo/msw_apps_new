# coding: utf-8

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from bottle import get, request, redirect, post
from bottle import jinja2_view as view
import time
import json

from service.activities_service import Activities_Service
from service.common_service import Common_Service
from service.yeepay_activities_service import Yeepay_Activities_Service
from service.weixin_activities_service import Weixin_Activities_Service
from service.config import ORDER_STATUS



@get('/activities/alipay/callback')
@view('templates/activities/success.html')
def alipay_callback():
    '''
    接收支付宝的回调，微信返回的格式如下:
    http://apps.mishangwo.com/alipay/callback?out_trade_no=T120150330005304A1B6&request_token=requestToken&result=success&trade_no=2015033000001000090046694226&sign=54c7b5e36dfced34383026a8c392f871&sign_type=MD5
    '''
    otn = request.query.get('out_trade_no', '')
    if not otn:
        redirect('/activities/pay_failed')

    result = request.query.get('result')
    if not result or result != 'success':
        redirect('/activities/pay_failed')

    time.sleep(2)

    order_service = Activities_Service()
    order_product = order_service.get_order_by_out_trade_no(otn)
    if not order_product:
        redirect('/activities/pay_failed?otn=%s' % otn)
    else:
        email = order_product.get('email', '')
        product_name = order_product.get('body', '')
        username = order_product.get('username', '')
        telephone = order_product.get('telephone', '')

        return {
            'order_date': time.strftime('%Y/%m/%d %H:%M', time.localtime()),
            'email': email,
            'username': username,
            'telephone': telephone,
            'product_name': product_name
        }



@post('/activities/alipay/notify')
def alipay_notify():
    '''
    接收支付宝的异步回调
    '''

    notify_data = request.forms.get('notify_data', '')
    if not notify_data:
        return

    notify_dict = Common_Service.xml_to_dict(notify_data)
    if notify_dict['trade_status'] in ['TRADE_SUCCESS', 'TRADE_FINISHED']:
        # 支付成功，更新数据库，并把订单信息传到Drupal后台
        order_service = Activities_Service()
        out_trade_no = notify_dict['out_trade_no']

        order = order_service.get_order_by_out_trade_no(out_trade_no)
        if not order or order['status'] == ORDER_STATUS.ALIPAY_SUCCEED:
            return 'success'
        order_service.update_order_status_by_otn(out_trade_no, ORDER_STATUS.ALIPAY_SUCCEED)
        return 'success'



@get('/activities/yeepay/fcallback')
@view('templates/activities/success.html')
def yeepay_fcallback():
    '''
    接收易宝的回调
    易宝的一键支付（包括PC版的一键支付和Mobile版的一键支付）成功后跳转的地址
    网银支付成功的跳转地址是/yeepay/netbank/notify
    http://apps.mishangwo.com/yeepay/fcallback?data=xxxxxx
    '''
    data = request.query.get('data', '')
    encryptkey = request.query.get('encryptkey', '')
    if not data or not encryptkey:
        redirect('/activities/pay_failed')

    yeepay_service = Yeepay_Activities_Service()
    data = yeepay_service.result_decrypt(data, encryptkey)
    data = json.loads(data)

    time.sleep(2)

    order_service = Activities_Service()
    order_product = order_service.get_order_by_out_trade_no(data['orderid'])
    if not order_product:
        redirect('/activities/pay_failed')
    else:
        email = order_product.get('email', '')
        product_name = order_product.get('body', '')
        username = order_product.get('username', '')
        telephone = order_product.get('telephone', '')

        return {
            'order_date': time.strftime('%Y/%m/%d %H:%M', time.localtime()),
            'email': email,
            'username': username,
            'telephone': telephone,
            'product_name': product_name
        }



@post('/activities/yeepay/callback')
def yeepay_callback():
    '''
    接收易宝的回调
    '''
    data = request.forms.get('data', '')
    encryptkey = request.forms.get('encryptkey', '')
    if not data or not encryptkey:
        return 'failed'

    yeepay_service = Yeepay_Activities_Service()
    data = yeepay_service.result_decrypt(data, encryptkey)
    data = json.loads(data)

    order_service = Activities_Service()
    out_trade_no = data['orderid']
    order = order_service.get_order_by_out_trade_no(out_trade_no)
    print 'order: %s' % order
    if not order or order['status'] == ORDER_STATUS.YEEPAY_SUCCEED:
        return 'success'
    order_service.update_order_status_by_otn(out_trade_no, ORDER_STATUS.YEEPAY_SUCCEED)
    return 'success'





@post('/activities/wxpay/notify')
def wxpay_notify():
    '''
    接收微信的回调，微信返回的格式如下:
    <?xml version="1.0" encoding="utf-8"?>
    <xml>
        <appid><![CDATA[wxf63bd1d65d623115]]></appid>
        <bank_type><![CDATA[CFT]]></bank_type>
        <cash_fee><![CDATA[1]]></cash_fee>
        <fee_type><![CDATA[CNY]]></fee_type>
        <is_subscribe><![CDATA[Y]]></is_subscribe>
        <mch_id><![CDATA[1230909702]]></mch_id>
        <nonce_str><![CDATA[1MhsY3mpN7bsuFA]]></nonce_str>
        <openid><![CDATA[oaheduL4sTSuQuo7zy3gG8T1UWsU]]></openid>
        <out_trade_no><![CDATA[T1201503221355587966]]></out_trade_no>
        <result_code><![CDATA[SUCCESS]]></result_code>
        <return_code><![CDATA[SUCCESS]]></return_code>
        <sign><![CDATA[93F20C5326CAF15EE83D832A6125BBD0]]></sign>
        <time_end><![CDATA[20150322135623]]></time_end>
        <total_fee>1</total_fee>
        <trade_type><![CDATA[JSAPI]]></trade_type>
        <transaction_id><![CDATA[1007550736201503220035499040]]></transaction_id>
    </xml>
    '''
    order_service = Activities_Service()
    content = request.body.readlines()
    content = ''.join(content)
    result = Weixin_Activities_Service.xml_to_dict(content)
    if result['return_code'] == 'SUCCESS':  # 通信成功
        if result['result_code'] == 'SUCCESS':  # 支付成功
            # 支付成功，更新数据库，并把订单信息传到Drupal后台
            out_trade_no = result['out_trade_no']
            order = order_service.get_order_by_out_trade_no(out_trade_no)
            if not order or order['status'] == ORDER_STATUS.WEIXIN_PAID_SUCCEED:
                return
            order_service.update_order_status_by_otn(out_trade_no, ORDER_STATUS.WEIXIN_PAID_SUCCEED)

            response = Weixin_Activities_Service.dict_to_xml(dict(return_code='SUCCESS', return_msg='OK'))
            return response
