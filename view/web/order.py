# coding: utf-8
'''
订单相关的页面
'''

from bottle import (
    get,
    request,
    redirect,
    abort
)
from bottle import jinja2_view as view
from hashlib import md5
import time
import base64
import urllib

from service.weixin_service import Weixin_Service
from service.drupal_service import Drupal_Service
from service.order_service import Order_Service
from service.alipay_service import Alipay_Service
from service.alipay_service import Alipay_Service_V2
from service.yeepay_service import Yeepay_Service
from service.common_service import Common_Service


@get('/pay/alipay')
@view('templates/order/pay_iframe.html')
def pay_alipay():
    url = request.query.get('url')
    if not url:
        abort(404, 'page not found')
        return

    return {
        'url': url
    }


@get('/order/pre_pay')
@view('templates/order/pre_pay.html')
def order_pre_pay():
    '''
    用户选择支付方式的页面。这个页面的前一个页面是商品展示页面
    到这个页面，意味着微信授权回来了，肯定有code参数
    '''
    wx_service = Weixin_Service()

    code = request.query.get('code')
    state = request.query.get('state')

    if not code or not state:
        return 'Invalid Request!'

    # 获取用户的OpenID
    access_token, openid = wx_service.get_oauth_access_token_and_openid(code)
    if not access_token or not openid:
        return 'Invalid Request!'

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'openid': openid,
        'sku': sku,
        'email': email
    }


@get('/order/pre_pay1')
@view('templates/order/pre_pay1.html')
def order_pre_pay_1():
    '''
    用户选择支付方式的页面。这个页面的前一个页面是商品展示页面
    到这个页面，意味着微信授权回来了，肯定有code参数
    '''
    wx_service = Weixin_Service()

    code = request.query.get('code')
    state = request.query.get('state')

    if not code or not state:
        return 'Invalid Request!'

    # 获取用户的OpenID
    access_token, openid = wx_service.get_oauth_access_token_and_openid(code)
    if not access_token or not openid:
        return 'Invalid Request!'

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'openid': openid,
        'sku': sku,
        'email': email
    }


@get('/order/pre_pay2')
@view('templates/order/pre_pay2.html')
def order_pre_pay_2():
    '''
    用户选择支付方式的页面。这个页面的前一个页面是商品展示页面
    到这个页面，意味着微信授权回来了，肯定有code参数
    '''
    wx_service = Weixin_Service()

    code = request.query.get('code')
    state = request.query.get('state')

    if not code or not state:
        return 'Invalid Request!'

    # 获取用户的OpenID
    access_token, openid = wx_service.get_oauth_access_token_and_openid(code)
    if not access_token or not openid:
        return 'Invalid Request!'

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'openid': openid,
        'sku': sku,
        'email': email
    }


@get('/order/pre_pay3')
@view('templates/order/pre_pay3.html')
def order_pre_pay_3():
    '''
    用户选择支付方式的页面。这个页面的前一个页面是商品展示页面
    到这个页面，意味着微信授权回来了，肯定有code参数
    '''
    wx_service = Weixin_Service()

    code = request.query.get('code')
    state = request.query.get('state')

    if not code or not state:
        return 'Invalid Request!'

    # 获取用户的OpenID
    access_token, openid = wx_service.get_oauth_access_token_and_openid(code)
    if not access_token or not openid:
        return 'Invalid Request!'

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'openid': openid,
        'sku': sku,
        'email': email
    }


@get('/order/pre_pay4')
@view('templates/order/pre_pay4.html')
def order_pre_pay_4():
    '''
    用户选择支付方式的页面。这个页面的前一个页面是商品展示页面
    到这个页面，意味着微信授权回来了，肯定有code参数
    '''
    wx_service = Weixin_Service()

    code = request.query.get('code')
    state = request.query.get('state')

    if not code or not state:
        return 'Invalid Request!'

    # 获取用户的OpenID
    access_token, openid = wx_service.get_oauth_access_token_and_openid(code)
    if not access_token or not openid:
        return 'Invalid Request!'

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'openid': openid,
        'sku': sku,
        'email': email
    }


@get('/order/pre_pay_alipay_test')
@view('templates/order/pre_pay_alipay_test.html')
def order_pre_pay_alipay_test():
    '''
    用户选择支付方式的页面。这个页面的前一个页面是商品展示页面
    到这个页面，意味着微信授权回来了，肯定有code参数
    '''
    wx_service = Weixin_Service()

    code = request.query.get('code')
    state = request.query.get('state')

    if not code or not state:
        return 'Invalid Request!'

    # 获取用户的OpenID
    access_token, openid = wx_service.get_oauth_access_token_and_openid(code)
    if not access_token or not openid:
        return 'Invalid Request!'

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'openid': openid,
        'sku': sku,
        'email': email
    }


@get('/order/pre_pay5')
@view('templates/order/pre_pay5.html')
def order_pre_pay_5():
    '''
    用户选择支付方式的页面。这个页面的前一个页面是商品展示页面
    到这个页面，意味着微信授权回来了，肯定有code参数
    '''

    state = request.query.get('state')

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'sku': sku,
        'email': email
    }


@get('/order/pre_pay6')
@view('templates/order/pre_pay6.html')
def order_pre_pay_6():
    state = request.query.get('state')

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'sku': sku,
        'email': email
    }


@get('/order/pre_pay7')
@view('templates/order/pre_pay7.html')
def order_pre_pay_7():
    '''
    用户选择支付方式的页面。这个页面的前一个页面是商品展示页面
    到这个页面，意味着微信授权回来了，肯定有code参数
    '''
    wx_service = Weixin_Service()

    code = request.query.get('code')
    state = request.query.get('state')

    if not code or not state:
        return 'Invalid Request!'

    # 获取用户的OpenID
    access_token, openid = wx_service.get_oauth_access_token_and_openid(code)
    if not access_token or not openid:
        return 'Invalid Request!'

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'openid': openid,
        'sku': sku,
        'email': email
    }


@get('/order/pre_paya')
@view('templates/order/pre_paya.html')
def order_pre_pay_a():
    state = request.query.get('state')

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'sku': sku,
        'email': email
    }


@get('/order/pre_payb')
@view('templates/order/pre_payb.html')
def order_pre_pay_b():
    state = request.query.get('state')

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'sku': sku,
        'email': email
    }


@get('/order/pre_payc')
@view('templates/order/pre_payc.html')
def order_pre_pay_c():
    state = request.query.get('state')

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'sku': sku,
        'email': email
    }


@get('/order/pre_payd')
@view('templates/order/pre_payd.html')
def order_pre_pay_d():
    state = request.query.get('state')

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'sku': sku,
        'email': email
    }


@get('/order/pre_pay_o1')
@view('templates/order/pre_pay_o1.html')
def order_pre_pay_o1():
    '''
    用户选择支付方式的页面。这个页面的前一个页面是商品展示页面
    到这个页面，意味着微信授权回来了，肯定有code参数
    '''

    state = request.query.get('state')

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'sku': sku,
        'email': email
    }


@get('/order/pre_pay_o2')
@view('templates/order/pre_pay_o2.html')
def order_pre_pay_o2():
    '''
    用户选择支付方式的页面。这个页面的前一个页面是商品展示页面
    到这个页面，意味着微信授权回来了，肯定有code参数
    '''

    state = request.query.get('state')

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'sku': sku,
        'email': email
    }


@get('/order/pre_pay_o3')
@view('templates/order/pre_pay_o3.html')
def order_pre_pay_o3():
    '''
    用户选择支付方式的页面。这个页面的前一个页面是商品展示页面
    到这个页面，意味着微信授权回来了，肯定有code参数
    '''

    state = request.query.get('state')

    sku_and_email = base64.decodestring(state)
    sku, email = sku_and_email.split(',', 1)
    return {
        'sku': sku,
        'email': email
    }


@get('/order/pay_succeed')
@view('templates/order/success.html')
def order_pay_succeed():
    '''
    支付成功
    目前这个页面用于微信支持成功后的跳转，alipay和yeepay的支付跳转页面分别为/apilay/return_url和yeepay/fcallback
    '''
    otn = request.query.get('otn', '')
    if not otn:
        redirect('/order/pay_failed')

    # 根据订单号调用查找跟这个订单相关的产品和用户信息
    # 产品和用户信息是依赖微信支付回调才会产生的，因此不能100%保证能
    # 拿到数据。为了增加成功的可能性，我们在这个页面sleep 2s再请求数据并返回
    time.sleep(2)

    order_service = Order_Service()
    order_product = order_service.get_order_product_by_out_trade_no(otn)
    if not order_product:
        redirect('/order/pay_failed?otn=%s' % otn)
    else:
        # 需要返回订单编号，产品信息，邮箱以及密码(如果有密码的话)
        order_key = order_product.get('order_key', '')
        email = order_product.get('email', '')
        password = order_product.get('password', '')
        product_name = order_product['products'][0]['name']
        if not password:
            password = u'(你的旧密码)'

        return {
            'order_key': order_key,
            'order_date': time.strftime('%Y/%m/%d %H:%M', time.localtime()),
            'email': email,
            'password': password,
            'is_android': Common_Service.is_android(),
            'is_ios': Common_Service.is_ios(),
            'product_name': product_name
        }


@get('/order/pay_failed')
@view('templates/order/failed.html')
def order_pay_failed():
    '''
    支付失败
    '''
    otn = request.query.get('otn', '')
    return {
        'otn': otn
    }


@get('/order/pay.htm')
@view('templates/order/pay.htm')
def order_pay_htm():
    '''
    支付宝跳转页面
    '''
    return {
        'is_android': Common_Service.is_android(),
        'is_ios': Common_Service.is_ios()
    }


@get('/order/pay_v2.htm')
@view('templates/order/pay_v2.htm')
def order_pay_v2_htm():
    '''
    支付宝跳转页面
    '''
    return {
        'is_android': Common_Service.is_android(),
        'is_ios': Common_Service.is_ios()
    }


@get('/api/order/create_weixin')
def api_create_weixin_order():
    '''
    创建微信订单
    '''
    wx_service = Weixin_Service()

    openid = request.query.get('openid')
    sku = request.query.get('sku')
    email = request.query.get('email')
    spbill_create_ip = request.environ.get('REMOTE_ADDR')
    test_group = request.query.get('test_group', '0')

    if not openid or not email or not sku or sku not in ['T1', 'T2']:
        return {
            'success': False,
            'err_msg': '创建订单失败！(没有SKU信息)',
            'sdkinfo': '',
            'payinfo': ''
        }

    # 在系统中创建订单
    succeed, payinfo = wx_service.create_prepay_order(email, openid, sku, spbill_create_ip, test_group)
    if succeed:
        sdkinfo = wx_service.get_jssdk_info(request.url)
        return {
            'success': True,
            'sdkinfo': sdkinfo,
            'payinfo': payinfo
        }
    else:
        return {
            'success': False,
            'err_msg': '创建订单失败！(请联系管理员)',
            'sdkinfo': '',
            'payinfo': ''
        }


@get('/api/order/create_alipay')
def api_create_alipay_order():
    '''
    创建支付宝订单
    '''
    alipay_service = Alipay_Service()
    order_service = Order_Service()

    openid = request.query.get('openid')
    sku = request.query.get('sku')
    email = request.query.get('email')
    test_group = request.query.get('test_group', '0')
    spbill_create_ip = request.environ.get('REMOTE_ADDR')

    if not email or not sku or sku not in ['T1', 'T2']:
        return {
            'success': False,
            'err_msg': '创建订单失败！(参数不完整)',
        }

    # 在系统中创建订单
    succeed, order_info = order_service.create_order(email, openid, sku, spbill_create_ip, 'alipay', test_group)
    if not succeed:
        return {
            'success': False,
            'err_msg': u'创建支付宝订单失败！'
        }

    # 根据订单信息，请求支付宝的授权接口，返回request token
    succeed, info = alipay_service.get_request_token(order_info)
    if not succeed:
        return {
            'success': False,
            'err_msg': info
        }

    # 获取Token成功，组建提交到支付宝的支付请求URL和数据，返回给客户端
    execute_url = alipay_service.get_alipay_execute_url(info)
    return {
        'success': True,
        'execute_url': execute_url
    }


@get('/api/order/create_alipay_v2')
def api_create_alipay_order_v2():
    '''
    使用V2接口创建支付宝订单
    '''
    alipay_service = Alipay_Service_V2()
    order_service = Order_Service()

    openid = request.query.get('openid')
    sku = request.query.get('sku')
    email = request.query.get('email')
    test_group = request.query.get('test_group', '0')
    spbill_create_ip = request.environ.get('REMOTE_ADDR')

    if not email or not sku or sku not in ['T1', 'T2']:
        return {
            'success': False,
            'err_msg': '创建订单失败！(参数不完整)',
        }

    # 在系统中创建订单
    succeed, order_info = order_service.create_order(email, openid, sku, spbill_create_ip, 'alipay', test_group)
    if not succeed:
        return {
            'success': False,
            'err_msg': u'创建支付宝订单失败！'
        }

    execute_url = alipay_service.get_alipay_execute_url(order_info)
    return {
        'success': True,
        'execute_url': execute_url
    }


@get('/api/order/create_alipay_iframe')
def api_create_alipay_iframe():
    '''
    在iframe创建支付宝订单
    '''
    time.sleep(2)
    alipay_service = Alipay_Service()
    order_service = Order_Service()

    openid = request.query.get('openid')
    sku = request.query.get('sku')
    email = request.query.get('email')
    test_group = request.query.get('test_group', '0')
    spbill_create_ip = request.environ.get('REMOTE_ADDR')

    if not email or not sku or sku not in ['T1', 'T2']:
        return {
            'success': False,
            'err_msg': '创建订单失败！(参数不完整)',
        }

    # 在系统中创建订单
    succeed, order_info = order_service.create_order(
        email, openid, sku, spbill_create_ip, 'alipay', test_group)
    if not succeed:
        return {
            'success': False,
            'err_msg': u'创建支付宝订单失败！'
        }

    # 根据订单信息，请求支付宝的授权接口，返回request token
    succeed, info = alipay_service.get_request_token(order_info)
    if not succeed:
        return {
            'success': False,
            'err_msg': info
        }

    # 获取Token成功，组建提交到支付宝的支付请求URL和数据，返回给客户端
    execute_url = alipay_service.get_alipay_execute_url(info)
    return {
        'success': True,
        'execute_url': urllib.quote_plus(execute_url)
    }


@get('/api/order/create_yeepay')
def api_create_yeepay_order():
    '''
    创建易宝支付订单
    '''
    yeepay_service = Yeepay_Service()
    order_service = Order_Service()

    openid = request.query.get('openid')
    sku = request.query.get('sku')
    email = request.query.get('email')
    spbill_create_ip = request.environ.get('REMOTE_ADDR')
    ua = request.headers.get('User-Agent', '')
    test_group = request.query.get('test_group', '0')

    if not email or not sku or sku not in ['T1', 'T2']:
        return {
            'success': False,
            'err_msg': '创建订单失败！(参数不完整)',
        }

    # 在系统中创建订单
    succeed, order_info = order_service.create_order(
        email, openid, sku, spbill_create_ip, 'yeepay', test_group)
    if not succeed:
        return {
            'success': False,
            'err_msg': u'创建易宝订单失败！'
        }

    # 获取Token成功，组建提交到易宝支付的支付请求URL，返回给客户端
    otn = order_info['out_trade_no']
    amount = order_info['total_fee']
    product_name = order_info['body']
    execute_url = yeepay_service.get_yeepay_execute_url(otn, amount, product_name, ua, spbill_create_ip, email)
    return {
        'success': True,
        'execute_url': execute_url
    }


@get('/api/order/create_in_drupal')
def api_order_create_in_drupal():
    '''
    把微信订单同步到Drupal
    '''

    dp_service = Drupal_Service()
    od_service = Order_Service()

    otn = request.query.get('otn', '')

    transaction_id = md5(str(time.time())).hexdigest().upper()

    order = od_service.get_order_by_out_trade_no(otn)
    if not order:
        return 'Order not exists!'

    # 在系统中创建订单
    dp_service.post_to_drupal(order, transaction_id, 'WeChat', 'JSAPI')


@get('/api/order/create_alipay_v2_iframe')
def api_create_alipay_v2_iframe():
    '''
    在iframe创建新版本的支付宝订单
    '''
    time.sleep(2)
    alipay_service = Alipay_Service_V2()
    order_service = Order_Service()

    openid = request.query.get('openid')
    sku = request.query.get('sku')
    email = request.query.get('email')
    test_group = request.query.get('test_group', '0')
    spbill_create_ip = request.environ.get('REMOTE_ADDR')

    if not email or not sku or sku not in ['T1', 'T2']:
        return {
            'success': False,
            'err_msg': '创建订单失败！(参数不完整)',
        }

    # 在系统中创建订单
    succeed, order_info = order_service.create_order(
        email, openid, sku, spbill_create_ip, 'alipay', test_group)
    if not succeed:
        return {
            'success': False,
            'err_msg': u'创建支付宝订单失败！'
        }

    execute_url = alipay_service.get_alipay_execute_url(order_info)
    print execute_url
    return {
        'success': True,
        'execute_url': urllib.quote_plus(execute_url)
    }
