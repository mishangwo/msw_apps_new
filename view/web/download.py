# encoding=utf8
'''
下载页面
'''

from bottle import get, redirect
from bottle import jinja2_view as view

from service.common_service import Common_Service


@get('/download/app')
def touting_d():
    if Common_Service.is_mobile():
        redirect('/download/mobile')
    else:
        redirect('/download/pc')


@get('/download/mobile')
@view('templates/touting/m_download.html')
def touting_d_mobile():
    '''
    Mobile版的纯下载页面
    '''
    return {}


@get('/download/pc')
@view('templates/touting/pc_download.html')
def touting_pc_mobile():
    '''
    PC版的纯下载页面
    '''
    return {}
