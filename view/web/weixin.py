# coding: utf-8
'''
微信相关的页面
'''

from bottle import get, request, redirect, post
import json

from service.weixin_service import Weixin_Service
from service.drupal_service import Drupal_Service
from service.order_service import Order_Service
from service.config import WX_APP_ID, ORDER_STATUS


@get('/weixin/oauth')
def weixin_oauth():
    wx_service = Weixin_Service()

    code = request.query.get('code')

    if not code:
        redirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=http://apps.mishangwo.com/weixin/oauth&response_type=code&scope=snsapi_base#wechat_redirect' % WX_APP_ID)
    else:
        # 获取用户的OpenID
        access_token, openid = wx_service.get_oauth_access_token_and_openid(code)

        # 在系统中创建订单
        order_id = wx_service.create_prepay_order(openid, 100, request.environ.get('REMOTE_ADDR'))
        return '订单已创建，请支付...(%s)' % str(order_id)


@post('/wxpay/notify')
def wxpay_notify():
    '''
    接收微信的回调，微信返回的格式如下:
    <?xml version="1.0" encoding="utf-8"?>
    <xml>
        <appid><![CDATA[wxf63bd1d65d623115]]></appid>
        <bank_type><![CDATA[CFT]]></bank_type>
        <cash_fee><![CDATA[1]]></cash_fee>
        <fee_type><![CDATA[CNY]]></fee_type>
        <is_subscribe><![CDATA[Y]]></is_subscribe>
        <mch_id><![CDATA[1230909702]]></mch_id>
        <nonce_str><![CDATA[1MhsY3mpN7bsuFA]]></nonce_str>
        <openid><![CDATA[oaheduL4sTSuQuo7zy3gG8T1UWsU]]></openid>
        <out_trade_no><![CDATA[T1201503221355587966]]></out_trade_no>
        <result_code><![CDATA[SUCCESS]]></result_code>
        <return_code><![CDATA[SUCCESS]]></return_code>
        <sign><![CDATA[93F20C5326CAF15EE83D832A6125BBD0]]></sign>
        <time_end><![CDATA[20150322135623]]></time_end>
        <total_fee>1</total_fee>
        <trade_type><![CDATA[JSAPI]]></trade_type>
        <transaction_id><![CDATA[1007550736201503220035499040]]></transaction_id>
    </xml>
    '''
    order_service = Order_Service()
    drupal_service = Drupal_Service()
    content = request.body.readlines()
    content = ''.join(content)
    result = Weixin_Service.xml_to_dict(content)
    if result['return_code'] == 'SUCCESS':  # 通信成功
        if result['result_code'] == 'SUCCESS':  # 支付成功
            # 支付成功，更新数据库，并把订单信息传到Drupal后台
            out_trade_no = result['out_trade_no']
            order = order_service.get_order_by_out_trade_no(out_trade_no)
            if order['sku'] == 'GAOSHOU_T2':
                order['sku'] = 'T2'
            if not order or order['status'] == ORDER_STATUS.WEIXIN_PAID_SUCCEED:
                return
            order_service.update_order_status_by_otn(out_trade_no, ORDER_STATUS.WEIXIN_PAID_SUCCEED)

            # 调用Drupal API，把订单传到Drupal后台并触发邮件
            ret = drupal_service.post_to_drupal(order, result['transaction_id'], 'WeChat', 'JSAPI')
            try:
                ret = json.loads(ret)
                order_service.save_order_product(ret)
            except Exception, e:
                print 'Error saving order products in view. (%s)' % str(e)

            response = Weixin_Service.dict_to_xml(dict(return_code='SUCCESS', return_msg='OK'))
            return response
