# encoding=utf8
'''
偷听女人心相关的一些页面
'''

from bottle import get, request, post, redirect
from bottle import jinja2_view as view

from service.config import WX_APP_ID
from service.common_service import Common_Service

import base64


@get('/touting/index')
@view('templates/touting/index.html')
def touting_index():
    return {}


@get('/touting/index1')
@view('templates/touting/index1.html')
def touting_index_1():
    '''
    只有支付宝，易宝两种支付，价格分别为30期299，45期399
    '''
    return {}


@get('/touting/index2')
@view('templates/touting/index2.html')
def touting_index_2():
    '''
    只有支付宝，易宝两种支付，价格分别为30期299，45期399
    '''
    return {}


@get('/touting/index3')
@view('templates/touting/index3.html')
def touting_index_3():
    '''
    只有支付宝，易宝两种支付，价格分别为30期299，45期399
    '''
    return {}


@get('/touting/index4')
@view('templates/touting/index4.html')
def touting_index_4():
    '''
    只有支付宝，易宝两种支付，价格分别为30期699，45期799
    '''
    return {}


@get('/touting/index_alipay_test')
@view('templates/touting/index_alipay_test.html')
def touting_index_alipay_test():
    '''
    测试新版支付宝接口
    '''
    return {}


@get('/touting/index5')
@view('templates/touting/index5.html')
def touting_index_5():
    '''
    只有支付宝，易宝两种支付，价格分别为30期299，45期398
    '''
    return {}


@get('/touting/index6')
@view('templates/touting/index6.html')
def touting_index_6():
    '''
    只有支付宝，易宝两种支付，价格分别为30期399，45期498
    '''
    return {}


@get('/touting/index7')
@view('templates/touting/index7.html')
def touting_index_7():
    '''
    支付宝，易宝和微信支付三种支付，价格分别为30期299，45期398
    '''
    return {}


@get('/touting/indexa')
@view('templates/touting/indexa.html')
def touting_index_a():
    return {}


@get('/touting/indexb')
@view('templates/touting/indexb.html')
def touting_index_b():
    return {}


@get('/touting/indexc')
@view('templates/touting/indexc.html')
def touting_index_c():
    return {}


@get('/touting/indexd')
@view('templates/touting/indexd.html')
def touting_index_d():
    return {}


@get('/touting/indexe')
@view('templates/touting/indexe.html')
def touting_index_e():
    return {}


@get('/touting/indexf')
@view('templates/touting/indexf.html')
def touting_index_f():
    return {}


@get('/touting/indexg')
@view('templates/touting/indexg.html')
def touting_index_g():
    return {}


@get('/touting/order1')
@view('templates/touting/index_o1.html')
def touting_order_1():
    '''
    只有支付宝，易宝两种支付，价格分别为30期299，45期398
    '''
    return {}


@get('/touting/order2')
@view('templates/touting/index_o2.html')
def touting_order_2():
    '''
    只有支付宝，易宝两种支付，价格分别为30期299，45期398
    '''
    return {}


@get('/touting/order3')
@view('templates/touting/index_o3.html')
def touting_order_3():
    '''
    只有支付宝，易宝两种支付，价格分别为30期299，45期398
    '''
    return {}


@get('/touting/ordera')
@view('templates/touting/index_oa.html')
def touting_order_a():
    '''
    只有支付宝，易宝两种支付，价格分别为30期299，45期398
    '''
    return {}


@get('/touting/orderb')
@view('templates/touting/index_ob.html')
def touting_order_b():
    '''
    只有支付宝，易宝两种支付，价格分别为30期299，45期398
    '''
    return {}


@get('/touting/orderc')
@view('templates/touting/index_oc.html')
def touting_order_c():
    '''
    只有支付宝，易宝两种支付，价格分别为30期299，45期398
    '''
    return {}


@get('/touting/orderd')
@view('templates/touting/index_od.html')
def touting_order_d():
    '''
    只有支付宝，易宝两种支付，价格分别为30期299，45期398
    '''
    return {}


@get('/touting/download')
def touting_download():
    '''
    点击下载
    '''
    if Common_Service.is_android():
        if Common_Service.is_wechat():
            redirect('http://a.app.qq.com/o/simple.jsp?pkgname=com.mishangwo')
        else:
            redirect('https://oss.mishangwo.com/apk/MSW_1_0_9.apk')
    else:
        redirect('http://a.app.qq.com/o/simple.jsp?pkgname=com.mishangwo&g_f=991653')


@post('/api/touting/get')
def api_touting_get():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'https://open.weixin.qq.com/connect/oauth2/authorize?state=%s&appid=%s&redirect_uri=http://apps.mishangwo.com/order/pre_pay?response_type=code&scope=snsapi_base#wechat_redirect' % (
        state, WX_APP_ID)
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/get1')
def api_touting_get1():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'https://open.weixin.qq.com/connect/oauth2/authorize?state=%s&appid=%s&redirect_uri=http://apps.mishangwo.com/order/pre_pay1?response_type=code&scope=snsapi_base#wechat_redirect' % (
        state, WX_APP_ID)
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/get2')
def api_touting_get2():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'https://open.weixin.qq.com/connect/oauth2/authorize?state=%s&appid=%s&redirect_uri=http://apps.mishangwo.com/order/pre_pay2?response_type=code&scope=snsapi_base#wechat_redirect' % (
        state, WX_APP_ID)
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/get3')
def api_touting_get3():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'https://open.weixin.qq.com/connect/oauth2/authorize?state=%s&appid=%s&redirect_uri=http://apps.mishangwo.com/order/pre_pay3?response_type=code&scope=snsapi_base#wechat_redirect' % (
        state, WX_APP_ID)
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/get4')
def api_touting_get4():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'https://open.weixin.qq.com/connect/oauth2/authorize?state=%s&appid=%s&redirect_uri=http://apps.mishangwo.com/order/pre_pay4?response_type=code&scope=snsapi_base#wechat_redirect' % (
        state, WX_APP_ID)
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/get_alipay_test')
def api_touting_get_alipay_test():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'https://open.weixin.qq.com/connect/oauth2/authorize?state=%s&appid=%s&redirect_uri=http://apps.mishangwo.com/order/pre_pay_alipay_test?response_type=code&scope=snsapi_base#wechat_redirect' % (
        state, WX_APP_ID)
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/get5')
def api_touting_get5():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'http://apps.mishangwo.com/order/pre_pay5?state=%s' % state
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/get6')
def api_touting_get6():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'http://apps.mishangwo.com/order/pre_pay6?state=%s' % state
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/get7')
def api_touting_get7():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'https://open.weixin.qq.com/connect/oauth2/authorize?state=%s&appid=%s&redirect_uri=http://apps.mishangwo.com/order/pre_pay7?response_type=code&scope=snsapi_base#wechat_redirect' % (
        state, WX_APP_ID)
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/geta')
def api_touting_geta():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'http://apps.mishangwo.com/order/pre_paya?state=%s' % state
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/getb')
def api_touting_getb():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'http://apps.mishangwo.com/order/pre_payb?state=%s' % state
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/getc')
def api_touting_getc():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'http://apps.mishangwo.com/order/pre_payc?state=%s' % state
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/getd')
def api_touting_getd():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'http://apps.mishangwo.com/order/pre_payd?state=%s' % state
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/get_o1')
def api_touting_get_o1():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'http://apps.mishangwo.com/order/pre_pay_o1?state=%s' % state
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/get_o2')
def api_touting_get_o2():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'http://apps.mishangwo.com/order/pre_pay_o2?state=%s' % state
    return {
        'success': True,
        'href': href
    }


@post('/api/touting/get_o3')
def api_touting_get_o3():
    email = request.forms.get('email', '')
    sku = request.forms.get('sku', '')
    if not email or not sku:
        return {
            'success': False,
            'err_msg': u'参数有误'
        }
    state = base64.encodestring('%s,%s' % (sku, email)).strip()
    href = 'http://apps.mishangwo.com/order/pre_pay_o3?state=%s' % state
    return {
        'success': True,
        'href': href
    }


@get('/touting/pay.htm')
@view('templates/order/pay.htm')
def order_pay_htm():
    '''
    支付宝跳转页面
    '''
    return {
        'is_android': Common_Service.is_android(),
        'is_ios': Common_Service.is_ios()
    }
