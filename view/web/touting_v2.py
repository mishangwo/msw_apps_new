# encoding=utf8
'''
偷听女人心第二版销售页面
'''

from bottle import get, request, post, redirect
from bottle import jinja2_view as view

from service.config import WX_APP_ID
from service.common_service import Common_Service

import base64


@get('/touting/ordere')
@view('templates/touting/index_oe.html')
def touting_order_e():
    return {}


@get('/touting/orderf')
@view('templates/touting/index_v2.html')
def touting_irder_f():
    return {}


@get('/touting/orderg')
@view('templates/touting/index_v3.html')
def touting_order_g():
    return {}


@get('/touting/order11')
@view('templates/touting/index_final.html')
def touting_irder_11():
    return {
        'group_name': 'touting_11'
    }


@get('/touting/order12')
@view('templates/touting/index_final.html')
def touting_irder_12():
    return {
        'group_name': 'touting_12'
    }


@get('/touting/order13')
@view('templates/touting/index_final.html')
def touting_irder_13():
    return {
        'group_name': 'touting_13'
    }


@get('/touting/order14')
@view('templates/touting/index_final.html')
def touting_irder_14():
    return {
        'group_name': 'touting_14'
    }


@get('/touting/order15')
@view('templates/touting/index_final.html')
def touting_irder_15():
    return {
        'group_name': 'touting_15'
    }


@get('/touting/order16')
@view('templates/touting/index_final.html')
def touting_irder_16():
    return {
        'group_name': 'touting_16'
    }


@get('/touting/order17')
@view('templates/touting/index_final.html')
def touting_irder_17():
    return {
        'group_name': 'touting_17'
    }


@get('/touting/order18')
@view('templates/touting/index_final.html')
def touting_irder_18():
    return {
        'group_name': 'touting_18'
    }


@get('/touting/order19')
@view('templates/touting/index_final.html')
def touting_irder_19():
    return {
        'group_name': 'touting_19'
    }


@get('/touting/order_iframe')
@view('templates/touting/index_final_iframe.html')
def touting_irder_19():
    return {
        'group_name': 'touting_iframe'
    }


@get('/touting/order_if_01')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_01():
    return {
        'group_name': 'touting_if_01'
    }


@get('/touting/order_if_02')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_02():
    return {
        'group_name': 'touting_if_02'
    }


@get('/touting/order_if_03')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_03():
    return {
        'group_name': 'touting_if_03'
    }


@get('/touting/order_if_04')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_04():
    return {
        'group_name': 'touting_if_04'
    }


@get('/touting/order_if_05')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_05():
    return {
        'group_name': 'touting_if_05'
    }


@get('/touting/order_if_06')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_06():
    return {
        'group_name': 'touting_if_06'
    }


@get('/touting/order_if_07')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_07():
    return {
        'group_name': 'touting_if_07'
    }


@get('/touting/order_if_08')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_08():
    return {
        'group_name': 'touting_if_08'
    }


@get('/touting/order_if_09')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_09():
    return {
        'group_name': 'touting_if_09'
    }

@get('/touting/order_if_10')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_10():
    return {
        'group_name': 'touting_if_10'
    }

@get('/touting/order_if_11')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_11():
    return {
        'group_name': 'touting_if_11'
    }

@get('/touting/order_if_12')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_12():
    return {
        'group_name': 'touting_if_12'
    }

@get('/touting/order_if_13')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_13():
    return {
        'group_name': 'touting_if_13'
    }

@get('/touting/order_if_14')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_14():
    return {
        'group_name': 'touting_if_14'
    }

@get('/touting/order_if_15')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_15():
    return {
        'group_name': 'touting_if_15'
    }

@get('/touting/order_if_16')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_16():
    return {
        'group_name': 'touting_if_16'
    }

@get('/touting/order_if_17')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_17():
    return {
        'group_name': 'touting_if_17'
    }

@get('/touting/order_if_18')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_18():
    return {
        'group_name': 'touting_if_18'
    }

@get('/touting/order_if_19')
@view('templates/touting/index_final_iframe.html')
def touting_order_iframe_19():
    return {
        'group_name': 'touting_if_19'
    }


@get('/touting/order_if_alipay_v2_01')
@view('templates/touting/index_final_iframe_with_new_alipay.html')
def touting_order_iframe_alipay_v2_01():
    return {
        'group_name': 'touting_if_alipay_v2_01'
    }
