# coding: utf-8
'''
支付宝回调相关的页面
'''

from bottle import get, request, redirect, post
from bottle import jinja2_view as view
import time
import json

from service.drupal_service import Drupal_Service
from service.common_service import Common_Service
from service.order_service import Order_Service

from service.config import ORDER_STATUS


@get('/alipay/callback')
@view('templates/order/success.html')
def alipay_callback():
    '''
    接收支付宝的回调，微信返回的格式如下:
    http://apps.mishangwo.com/alipay/callback?out_trade_no=T120150330005304A1B6&request_token=requestToken&result=success&trade_no=2015033000001000090046694226&sign=54c7b5e36dfced34383026a8c392f871&sign_type=MD5
    '''
    otn = request.query.get('out_trade_no', '')
    if not otn:
        redirect('/order/pay_failed')

    result = request.query.get('result')
    if not result or result != 'success':
        redirect('/order/pay_failed')

    time.sleep(2)

    order_service = Order_Service()
    order_product = order_service.get_order_product_by_out_trade_no(otn)
    if not order_product:
        redirect('/order/pay_failed')
    else:
        # 需要返回订单编号，产品信息，邮箱以及密码(如果有密码的话)
        order_key = order_product.get('order_key', '')
        email = order_product.get('email', '')
        password = order_product.get('password', '')
        product_name = order_product['products'][0]['name']
        if not password:
            password = u'(你的旧密码)'

        return {
            'order_key': order_key,
            'order_date': time.strftime('%Y/%m/%d %H:%M', time.localtime()),
            'email': email,
            'password': password,
            'product_name': product_name
        }


@get('/alipay/return_url')
@view('templates/order/success.html')
def alipay_return_url():
    '''
    接收新版支付宝API的回调
    '''
    query_string = request.query_string
    if not Common_Service.is_mobile():
        redirect('/gateway/pay_succeed_pc/alipay?%s' % query_string)

    otn = request.query.get('out_trade_no', '')
    if not otn:
        redirect('/order/pay_failed')

    is_success = request.query.get('is_success')
    if not is_success or is_success.lower() != 't':
        redirect('/order/pay_failed')

    time.sleep(2)

    order_service = Order_Service()
    order_product = order_service.get_order_product_by_out_trade_no(otn)
    if not order_product:
        redirect('/order/pay_failed')
    else:
        # 需要返回订单编号，产品信息，邮箱以及密码(如果有密码的话)
        order_key = order_product.get('order_key', '')
        email = order_product.get('email', '')
        password = order_product.get('password', '')
        product_name = order_product['products'][0]['name']
        if not password:
            password = u'(你的旧密码)'

        return {
            'order_key': order_key,
            'order_date': time.strftime('%Y/%m/%d %H:%M', time.localtime()),
            'email': email,
            'password': password,
            'product_name': product_name
        }


@post('/alipay/notify')
def alipay_notify():
    '''
    接收支付宝的异步回调
    '''

    notify_data = request.forms.get('notify_data', '')
    if not notify_data:
        return

    notify_dict = Common_Service.xml_to_dict(notify_data)
    if notify_dict['trade_status'] in ['TRADE_SUCCESS', 'TRADE_FINISHED']:
        # 支付成功，更新数据库，并把订单信息传到Drupal后台
        order_service = Order_Service()
        drupal_service = Drupal_Service()

        out_trade_no = notify_dict['out_trade_no']

        order = order_service.get_order_by_out_trade_no(out_trade_no)
        if not order or order['status'] == ORDER_STATUS.ALIPAY_SUCCEED:
            return 'success'
        order_service.update_order_status_by_otn(out_trade_no, ORDER_STATUS.ALIPAY_SUCCEED)

        # 调用Drupal API，把订单传到Drupal后台并触发邮件
        ret = drupal_service.post_to_drupal(order, notify_dict['trade_no'], 'Alipay', 'Wap')
        try:
            ret = json.loads(ret)
            order_service.save_order_product(ret)
        except Exception, e:
            print 'Error saving order products in view. (%s)' % str(e)
        return 'success'


@post('/alipay/notify_v2')
def alipay_notify_v2():
    '''
    接收新版支付宝API的异步回调
    '''

    out_trade_no = request.forms.get('out_trade_no')
    trade_no = request.forms.get('trade_no')
    trade_status = request.forms.get('trade_status')
    if not out_trade_no or not trade_status:
        return

    if trade_status in ['TRADE_SUCCESS', 'TRADE_FINISHED']:
        # 支付成功，更新数据库，并把订单信息传到Drupal后台
        order_service = Order_Service()
        drupal_service = Drupal_Service()

        order = order_service.get_order_by_out_trade_no(out_trade_no)
        if not order or order['status'] == ORDER_STATUS.ALIPAY_SUCCEED:
            return 'success'
        order_service.update_order_status_by_otn(out_trade_no, ORDER_STATUS.ALIPAY_SUCCEED)

        # 调用Drupal API，把订单传到Drupal后台并触发邮件
        ret = drupal_service.post_to_drupal(order, trade_no, 'Alipay', 'Wap')
        try:
            ret = json.loads(ret)
            order_service.save_order_product(ret)
        except Exception, e:
            print 'Error saving order products in view. (%s)' % str(e)
        return 'success'
