# coding: utf-8
'''
易宝回调相关的页面
'''

from bottle import get, request, redirect, post, route
from bottle import jinja2_view as view
import json
import time

from service.drupal_service import Drupal_Service
from service.order_service import Order_Service
from service.yeepay_service import Yeepay_Service
from service.common_service import Common_Service

from service.config import ORDER_STATUS


@get('/yeepay/fcallback')
@view('templates/order/success.html')
def yeepay_fcallback():
    '''
    接收易宝的回调
    易宝的一键支付（包括PC版的一键支付和Mobile版的一键支付）成功后跳转的地址
    网银支付成功的跳转地址是/yeepay/netbank/notify
    http://apps.mishangwo.com/yeepay/fcallback?data=xxxxxx
    '''
    query_string = request.query_string
    if not Common_Service.is_mobile():
        redirect('/gateway/pay_succeed_pc/yeepay?%s' % query_string)
    else:
        redirect('/gateway/pay_succeed_mobile/yeepay?%s' % query_string)


@route('/yeepay/netbank/notify', methods=['POST', 'GET'])
@view('templates/order/success.html')
def yeepay_netbank_post():
    '''
    接收易宝网银支付的后台通知回调
    '''
    query_string = request.query_string
    if not Common_Service.is_mobile():
        redirect('/gateway/pay_succeed_pc/yeepay_netbank?%s' % query_string)
    else:
        redirect('/gateway/pay_succeed_mobile/yeepay_netbank?%s' % query_string)


@post('/yeepay/callback')
def yeepay_callback():
    '''
    接收易宝的回调
    '''
    data = request.forms.get('data', '')
    encryptkey = request.forms.get('encryptkey', '')
    if not data or not encryptkey:
        return 'failed'

    yeepay_service = Yeepay_Service()
    data = yeepay_service.result_decrypt(data, encryptkey)
    data = json.loads(data)

    order_service = Order_Service()
    drupal_service = Drupal_Service()

    # 支付成功，更新数据库，并把订单信息传到Drupal后台
    out_trade_no = data['orderid']
    order = order_service.get_order_by_out_trade_no(out_trade_no)
    print 'order: %s' % order
    if not order or order['status'] == ORDER_STATUS.YEEPAY_SUCCEED:
        return 'success'
    order_service.update_order_status_by_otn(out_trade_no, ORDER_STATUS.YEEPAY_SUCCEED)

    # 调用Drupal API，把订单传到Drupal后台并触发邮件
    ret = drupal_service.post_to_drupal(order, data['yborderid'], 'Yeepay', 'Mobile')
    try:
        ret = json.loads(ret)
        order_service.save_order_product(ret)
    except Exception, e:
        print 'Error saving order products in view. (%s)' % str(e)
    return 'success'
