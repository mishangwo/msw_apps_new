# flake8: noqa

from front import *
from static import *
from touting import *
from touting_v2 import *
from weixin import *
from alipay import *
from yeepay import *
from order import *
from download import *