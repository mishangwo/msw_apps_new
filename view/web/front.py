# coding: utf-8

from bottle import get, error, request, abort
from bottle import jinja2_view as view


@get('/')
@get('/index')
@view('templates/index.html')
def index():
    return {}


@get('/contact')
@view('templates/contact.html')
def contact():
    return {}


@get('/product')
@view('templates/product.html')
def product():
    return {}


@get('/about')
@view('templates/about.html')
def about():
    return {}


@get('/privacy')
@view('templates/privacy.html')
def privacy():
    return {}


@get('/ping')
def ping():
    return 'ok'


@error(404)
@view('templates/not_found.html')
def error_404(error):
    return {}


@get('/jump')
@view('templates/jump.html')
def jump():
    url = request.query.get('url')
    if not url:
        abort(404, '')
    return {
        'url': url
    }
