# encoding=utf8
'''
静态文件相关的一些方法
'''

from bottle import get, static_file


@get('/robots.txt')
def send_robots():
    return static_file('robots.txt', root='./static/')


@get('/MP_verify_zGnUskr6wOap3sK1.txt')
def send_robots():
    return static_file('MP_verify_zGnUskr6wOap3sK1.txt', root='./static/')


@get('/assets/<filepath:path>')
def send_static_image(filepath):
    return static_file(filepath, root='./static/')


@get('/icon')
def send_robots():
    return static_file('favicon.png', root='./static/')
