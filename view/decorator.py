# coding: utf-8
import urlparse
import functools

def _patch_url_for(prefix, filename):
    return urlparse.urljoin(prefix, filename)

patched_funcs = {
    'url_for': _patch_url_for
}

def add_patch_funcs(fn):
    @functools.wraps(fn)
    def wrapped(*args, **kwargs):
        result = fn(*args, **kwargs)  #  result must be a dict
        if type(result) != type({}):
            return result

        result.update(patched_funcs)
        return result
    return wrapped

