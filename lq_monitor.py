# coding: utf-8

import socket
try:
    import simplejson as json
except ImportError:
    import json
import time
import datetime
import sys
import subprocess


def _print_msg(msg):
    print '[%s] %s' % (datetime.datetime.now(), msg)


def get_lq(addr):
    """
    采样Listen Queue，取5次数据，每隔2秒钟取一次，然后计算平均值
    """
    sample_list = []
    for i in range(5):
        _str = ''
        try:
            s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            s.connect(addr)
            while True:
                data = s.recv(4096)
                if len(data) < 1:
                    break
                _str += data.decode('utf8')
            uwsgi_info = json.loads(_str)
            lq = uwsgi_info.get('listen_queue', 0)
            _print_msg('Round %d Listen Queue: %s' % (i, lq))
            sample_list.append(lq)
            s.close()
            time.sleep(2)
        except Exception, e:
            _print_msg("Unable to get uWSGI statistics. [%s]" % str(e))

    if sample_list:
        return sum(sample_list) / len(sample_list)
    return -1


def restart_uwsgi():
    ret_code = subprocess.call('/home/deploy/msw.apps/restart_uwsgi')
    return ret_code


if __name__ == '__main__':
    argc = len(sys.argv)
    _print_msg('Start to checking lq status...')

    if argc < 2:
        _print_msg("You have to specify the uWSGI stats socket")
        sys.exit()

    avg_lq = get_lq(sys.argv[1])
    if avg_lq > 20:
        _print_msg('Average LQ is %s, need to restart uwsgi...' % avg_lq)
        ret_code = restart_uwsgi()
        _print_msg('Done with ret_code = %s' % ret_code)
    else:
        _print_msg('Average LQ is %s, it\'s safe.' % avg_lq)
