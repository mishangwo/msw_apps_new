# coding: utf-8

from view import *  # noqa
from bottle import default_app, run

if __name__ == '__main__':
    app = default_app()
    run(host='0.0.0.0', port=8080, debug=True, reloader=True, app=app)
else:
    application = default_app()
