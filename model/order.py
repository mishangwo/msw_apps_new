# encoding=utf8
'''
该文件说明订单相关的数据结构，并没有什么实际的作用
'''


class Order(object):
    appid = ''  # 微信APP ID
    mch_id = ''  # 微信商户ID
    nonce_str = ''  # 微信支付时随机生成的字符串
    body = ''  # 商品名字或描述
    openid = ''  # 购买的用户的微信ID
    out_trade_no = ''  # 订单的本地订单号
    total_fee = ''  # 订单价格
    spbill_create_ip = ''  # 用户的IP
    notify_url = ''  # 微信支付回调
    trade_type = ''  # 微信支付时提交的交易类型
    create_at = ''  # 创建时间
    sign = ''  # 微信支付时生成的签名串
    _type = ''  # 订单类型。weixin, baidu或者alipay
    status = ''  # 订单状态。1为新建，2为调用微信统一下单API成功, 3为微信支付成功
    prepay_id = ''  # 微信统一下单成功后返回的预支付交易会话标识
    paid_at = ''  # 订单更新时间
    email = ''  # 购买人的Email地址
    sku = ''  # 购买商品的SKU信息


class Order_Product(object):
    '''
    和订单相关的产品信息
    这个数据在调用Drupal创建订单成功后，由Drupal返回
    '''
    order_id = ''  # 商户端订单号，这个就是订单记录里的out_trade_no
    oid = ''
    uid = ''  # 用户ID
    order_key = ''  # 订单编号
    status = ''
    gateway = ''
    amount = ''
    total = ''
    currency = ''
    email = ''
    products = [
        {
            'opid': '',
            'oid': '',
            'name': '',
            'module': '',
            'type': '',
            'qty': '',
            'price': '',
            'sku': '',
            'id': '',
            'data': ''
        }
    ]
