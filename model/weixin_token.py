#encoding=utf8

class WeixinToken:
    token = '' # 获取的微信Token值
    expires_at = '' # 该Token过期时间

class JSTicket:
    ticket = '' # Ticket信息
    expires_at = '' # 该Ticket过期的时间
