deploy:
	rsync -avzP --exclude=.git --exclude=.venv --exclude=makefile --exclude=.gitignore --exclude=*.pyc --exclude=*.swp ./* msw_apps:/home/deploy/msw.apps/
	rsync -avzP --exclude=.git --exclude=.venv --exclude=makefile --exclude=.gitignore --exclude=*.pyc --exclude=*.swp ./* msw_apps2:/home/deploy/msw.apps/
	ssh msw_apps "cd /home/deploy && chown deploy:deploy msw.apps -R && cd msw.apps && ./reload_uwsgi"
	ssh msw_apps2 "cd /home/deploy && chown deploy:deploy msw.apps -R && cd msw.apps && ./reload_uwsgi"
